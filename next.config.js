const compose = require('next-compose');
module.exports = compose([
    {
        webpack(config, options) {
            config.module.rules.push(
                {
                    test: /\.mp3$/,
                    use: {
                        loader: 'file-loader',
                    },
                },
                {
                    test: /\.(graphql|gql)$/,
                    exclude: /node_modules/,
                    loader: 'graphql-tag/loader',
                },
                {
                    test: /\.svg$/,
                    use: ['@svgr/webpack'],
                },
                {
                    test: /\.(png|jpe?g|gif)$/i,
                    use: [
                        {
                            loader: 'file-loader',
                        },
                    ],
                }
            );
            return config;
        },
        webpackDevMiddleware: config => {
            return config;
        },
    },
]);
