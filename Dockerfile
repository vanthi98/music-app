
FROM node:12.20.1
WORKDIR /workspace/music-app
ADD package*.json ./ 
ADD yarn.lock ./
RUN yarn install
COPY . .
ENTRYPOINT ["sh", "./entrypoint.sh"]
EXPOSE 3000
