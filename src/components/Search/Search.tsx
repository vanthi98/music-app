import React, { FC } from 'react';
import classes from './styles.module.scss';
import { useRouter } from 'next/router';
import { useQuery } from '@apollo/react-hooks';
import GET_ALL_SONG_QUERY from '@/queries/GetAllSong.graphql';
import GET_SONG_BY_AUTHOR from '@/queries/queries/GetSongByAuthor.graphql';
import Song from '@/components/Song';
import { Row, Col, Spin, Empty, Tabs } from 'antd';

const { TabPane } = Tabs;

const Search: FC = () => {
    const router = useRouter();
    const { keyword } = router.query;
    const {
        data: songs,
        loading: songsLoading,
        error: songsError,
        refetch: songRefetch,
    } = useQuery(GET_ALL_SONG_QUERY, {
        fetchPolicy: 'no-cache',
        variables: {
            keyword,
        },
    });

    const {
        data: songsBA,
        loading: songsBALoading,
        error: songsBAError,
        refetch: songsBARefetch,
    } = useQuery(GET_SONG_BY_AUTHOR, {
        fetchPolicy: 'no-cache',
        variables: {
            author: keyword,
        },
    });

    const refetch = () => {
        songRefetch();
        songsBARefetch();
    };

    if (songsLoading || songsBALoading) {
        return <Spin tip="Get song..."></Spin>;
    }

    if (songsError || songsBAError) {
        return <div>Fetch song error...</div>;
    }

    if (!songs || !songsBA) {
        return <div>Fetch song error...</div>;
    }

    const data = songs.getAllSong;
    const dataBA = songsBA.getSongByAuthor;

    console.log('[data]', data);

    function callback(key) {
        console.log(key);
    }

    return (
        <div className={classes.root}>
            <h1 className={classes.title}>
                Tìm kiếm với từ khóa{' '}
                <span style={{ color: 'blue' }}>"{keyword}"</span>
            </h1>
            <Tabs defaultActiveKey="1" onChange={callback}>
                <TabPane tab="Tìm theo bài hát" key="song">
                    {data.length > 0 ? (
                        <Row
                            gutter={[
                                { xs: 8, sm: 16, md: 24, lg: 32 },
                                { xs: 8, sm: 16, md: 24, lg: 32 },
                            ]}
                        >
                            {data.map((song, index) => {
                                return (
                                    <Col xs={24} sm={24} md={12} lg={8} xl={6}>
                                        <Song data={song} refetch={refetch} />
                                    </Col>
                                );
                            })}
                        </Row>
                    ) : (
                        <div className={classes.empty}>
                            <Empty
                                image="/static/images/song-placeholder.png"
                                imageStyle={{
                                    height: 160,
                                }}
                                description={
                                    <h1 className={classes.empty_text}>
                                        Không tìm thấy bất cứ kết quả nào phù
                                        hợp
                                    </h1>
                                }
                            ></Empty>
                        </div>
                    )}
                </TabPane>
                <TabPane tab="Tìm theo tác giả" key="authors">
                    {dataBA.length > 0 ? (
                        <Row
                            gutter={[
                                { xs: 8, sm: 16, md: 24, lg: 32 },
                                { xs: 8, sm: 16, md: 24, lg: 32 },
                            ]}
                        >
                            {dataBA.map((song, index) => {
                                return (
                                    <Col xs={24} sm={24} md={12} lg={8} xl={6}>
                                        <Song data={song} refetch={refetch} />
                                    </Col>
                                );
                            })}
                        </Row>
                    ) : (
                        <div className={classes.empty}>
                            <Empty
                                image="/static/images/song-placeholder.png"
                                imageStyle={{
                                    height: 160,
                                }}
                                description={
                                    <h1 className={classes.empty_text}>
                                        Không tìm thấy bất cứ kết quả nào phù
                                        hợp
                                    </h1>
                                }
                            ></Empty>
                        </div>
                    )}
                </TabPane>
            </Tabs>
        </div>
    );
};

export default Search;
