import React, { FC, useEffect } from 'react';
import classes from './styles.module.scss';
import { Button, Row, Col, Modal, notification } from 'antd';
import { useQuery, useMutation } from '@apollo/react-hooks';
import LoadingIndicator from '@/components/base/loadingIndicator';
import SongItem from '@/components/UploadSong/Item';
import GET_SONG_BY_PLAYLIST from '@/queries/GetSongByPlaylist.graphql';
import GET_PLAYLIST_BY_ID from '@/queries/GetPlaylistById.graphql';
import DELETE_SONG_FROM_PLAYLIST from '@/queries/RemoveSongFromPlaylist.graphql';
import { useRouter } from 'next/router';

const PlaylistDetail: FC<any> = () => {
    const router = useRouter();
    const { id } = router.query;
    const { data: songs, loading, error, refetch } = useQuery(
        GET_SONG_BY_PLAYLIST,
        {
            fetchPolicy: 'network-only',
            variables: {
                playlist_id: id,
            },
        }
    );

    const {
        data: playlist,
        loading: playlistLoading,
        error: playlistError,
    } = useQuery(GET_PLAYLIST_BY_ID, {
        fetchPolicy: 'network-only',
        variables: {
            playlist_id: id,
        },
    });

    const [
        deleteSongMutation,
        { loading: deletePlaylistLoading, error: deletePlaylistError },
    ] = useMutation(DELETE_SONG_FROM_PLAYLIST);

    const handleDeleteSong = song_id => {
        Modal.confirm({
            content: 'Bạn có chắc muốn xóa bài hát này khỏi playlist?',
            async onOk() {
                await deleteSongMutation({
                    variables: {
                        playlist_id: id,
                        song_id,
                    },
                });
                if (refetch) {
                    refetch();
                }
                notification.success({
                    message: `Xóa bài hát thành công`,
                });
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    };

    useEffect(() => {
        refetch();
    }, []);

    if (loading || playlistLoading)
        return <LoadingIndicator loadingTitle="loading playlist" />;
    if (error || playlistError)
        return <div className={classes.error_page}>Error when fetch data</div>;
    if (!songs || !playlist) {
        return <div className={classes.error_page}>Error when fetch data</div>;
    }

    let list: JSX.Element;

    if (songs && playlist) {
        const playlistData = playlist.getPlaylistById;
        list = (
            <div className={classes.content__list} id="list">
                <h1 className={classes.title}>{playlistData.name}</h1>
                <Button>Thêm bài hát vào playlist</Button>
                {songs.getSongByPlaylist.map(song => {
                    return (
                        <Row
                            align="middle"
                            gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}
                        >
                            <Col span={22}>
                                <SongItem
                                    data={song}
                                    addtoplaylist="disable"
                                    refetch={refetch}
                                ></SongItem>
                            </Col>
                            <Col span={2}>
                                <Button
                                    className={classes.delete_button}
                                    type="primary"
                                    danger
                                    onClick={() => handleDeleteSong(song._id)}
                                >
                                    Delete
                                </Button>
                            </Col>
                        </Row>
                    );
                })}
            </div>
        );
    }

    return <div className={classes.root}>{list}</div>;
};

export default PlaylistDetail;
