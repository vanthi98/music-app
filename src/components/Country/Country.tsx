import React, { FC, useState, useEffect } from 'react';
import classes from './styles.module.scss';
import { useQuery } from '@apollo/react-hooks';
import { Row, Col } from 'antd';
import LoadingIndicator from '@/components/base/loadingIndicator';
import GET_ALL_COUNTRY from '@/queries/queries/GetAllCountries.graphql';
import CountryItem from './CountryItem';

const Country: FC = () => {
    const { data: countries, loading, error, refetch } = useQuery(
        GET_ALL_COUNTRY,
        {
            fetchPolicy: 'network-only',
        }
    );

    useEffect(() => {
        refetch();
    }, []);

    if (loading) return <LoadingIndicator loadingTitle="loading history" />;
    if (error)
        return <div className={classes.error_page}>Error when fetch data</div>;
    if (!countries) {
        return <div className={classes.error_page}>Error when fetch data</div>;
    }

    let list: JSX.Element;

    if (countries) {
        list = (
            <div className={classes.content__list} id="list">
                <h1 className={classes.title}>Danh sách quốc gia</h1>
                <Row
                    gutter={[
                        { xs: 16, sm: 24, md: 32, lg: 40 },
                        { xs: 16, sm: 24, md: 32, lg: 40 },
                    ]}
                    style={{ marginTop: '2rem' }}
                >
                    {countries.getAllCountry.length > 0 ? (
                        countries.getAllCountry.map(country => {
                            return (
                                <Col xs={12} sm={12} md={8} lg={6} xl={4}>
                                    <CountryItem
                                        countries={country}
                                        refetch={refetch}
                                    />
                                </Col>
                            );
                        })
                    ) : (
                        <h1 className={classes.no_country_text}>
                            Không tìm thấy quốc gia nào
                        </h1>
                    )}
                </Row>
            </div>
        );
    }

    return <div className={classes.root}>{list}</div>;
};

export default Country;
