import React, { FC, useEffect } from 'react';
import classes from './styles.module.scss';
import { ICountryItemProps } from './CountryItem.type';
import GET_SONG from '@/queries/GetSong.graphql';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { Dropdown, Menu, Modal, notification } from 'antd';
import Icon from '@ant-design/icons';
import InfoSvg from '@public/static/icons/info.svg';
import ActionSvg from '@public/static/icons/action.svg';
import TrashSvg from '@public/static/icons/trash.svg';
import { useRouter } from 'next/router';

const { Item } = Menu;

const PlaylistItem: FC<ICountryItemProps> = ({
    countries,
    refetch,
}): JSX.Element => {
    const router = useRouter();
    const { name, listSong, _id, country_id } = countries;
    const { data: song, loading, error } = useQuery(GET_SONG, {
        fetchPolicy: 'network-only',
        variables: {
            song_id: listSong[0],
        },
    });

    const handleViewDetail = () => {
        router.push(`/country-detail/${country_id}`);
    };

    const thumbnail = (): JSX.Element => {
        if (error || loading || !song)
            return (
                <img
                    src="https://via.placeholder.com/500"
                    className={classes.image}
                />
            );

        if (song) {
            return (
                <img
                    src={song.getSong.song_image_url}
                    className={classes.image}
                />
            );
        }
    };

    const menu: JSX.Element = (
        <Menu>
            <Item key="detail" onClick={handleViewDetail}>
                <Icon component={InfoSvg} />
                Danh sách bài hát
            </Item>
        </Menu>
    );

    return (
        <div className={classes.root}>
            <div className={classes.playlist}>
                <div
                    className={classes.playlist__info}
                    onClick={handleViewDetail}
                >
                    <span className={classes.playlist__name}>{name}</span>
                    <Dropdown overlay={menu} trigger={['click']}>
                        <a
                            className={classes.action}
                            onClick={e => e.preventDefault()}
                        >
                            <Icon component={ActionSvg} />
                        </a>
                    </Dropdown>
                </div>
            </div>
        </div>
    );
};

export default PlaylistItem;
