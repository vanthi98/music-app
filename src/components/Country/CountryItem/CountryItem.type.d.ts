interface ICountry {
    name: string;
    listSong: Array<string>;
    country_id?: string;
    _id: string;
}

export interface ICountryItemProps {
    countries: ICountry;
    refetch: Function;
}
