import classes from './styles.module.scss';
import React, { FC, useEffect, useRef } from 'react';
import { useProfile } from '@/containers/profiles/useProfile';
import { Spin, Avatar, Form, Input, Button, Tooltip, Row, Col } from 'antd';
import CREATE_MESSAGE from '@/queries/mutations/CreateMessage.graphql';
import GET_ROOM_DETAIL from '@/queries/queries/GetRoomDetail.graphql';
import MESSAGE_SEND from '@/queries/subscriptions/MessageSend.graphql';
import { useMutation, useSubscription, useQuery } from '@apollo/react-hooks';
import moment from 'moment';
import ScrollToBottom from 'react-scroll-to-bottom';

const ChatPopup: FC<any> = props => {
    const { data } = props;
    const { room_id } = data;
    const hookProps = useProfile({});
    const { profileData, profileId, isLoading } = hookProps;

    const [form] = Form.useForm();

    const {
        data: roomDetailData,
        loading: getRoomDetailLoading,
        error: getRoomDetailError,
        refetch,
    } = useQuery(GET_ROOM_DETAIL, {
        variables: {
            room_id,
        },
    });

    const [createMessage, { loading, error }] = useMutation(CREATE_MESSAGE);

    const { data: message_send } = useSubscription(MESSAGE_SEND, {
        variables: {
            profile_id: profileId,
        },
    });

    useEffect(() => {
        if (refetch) {
            refetch();
        }
    }, [message_send]);

    const AvatarComponent: FC<any> = ({ data }): JSX.Element => {
        if (data) {
            const { avatarUrl, account_name } = data;
            if (!avatarUrl)
                return (
                    <Avatar size={40}>
                        <p className={classes.avatar_text}>
                            {account_name[0].toUpperCase()}
                        </p>
                    </Avatar>
                );
            return <Avatar src={data && data.avatarUrl} size={40} />;
        }
    };

    if (isLoading || getRoomDetailLoading) return <Spin></Spin>;

    if (getRoomDetailError) return <div>Data fetch error</div>;

    if (!profileData || !profileId || !roomDetailData)
        return <div>Data fetch error</div>;

    return (
        <div className={classes.root}>
            <ScrollToBottom className={classes.message_container}>
                <div className={classes.message_content}>
                    {roomDetailData.getRoomDetail.messages.map(message => {
                        if (
                            profileData.account_name ===
                            message.user.account_name
                        )
                            return (
                                <div
                                    className={`${classes.message} ${classes.message_self}`}
                                >
                                    <div className={classes.message_chat}>
                                        <p>{message.content}</p>
                                        <p className={classes.time}>
                                            {moment(message.createdAt).format(
                                                'DD MM YYYY hh:mm:ss'
                                            )}
                                        </p>
                                    </div>
                                </div>
                            );
                        return (
                            <div className={classes.message}>
                                <div style={{ width: '100%' }}>
                                    <Tooltip
                                        placement="top"
                                        title={message.user.account_name}
                                    >
                                        {' '}
                                        <AvatarComponent data={message.user} />
                                    </Tooltip>
                                    <div className={classes.message_chat}>
                                        <p>{message.content}</p>
                                        <p className={classes.time}>
                                            {moment(message.createdAt).format(
                                                'DD MM YYYY hh:mm:ss'
                                            )}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </ScrollToBottom>
            <Form
                form={form}
                onFinish={async formValues => {
                    await createMessage({
                        variables: { content: formValues.chat, room_id },
                    });
                    refetch();
                    form.resetFields();
                }}
                layout="inline"
                className={classes.form}
            >
                <Row style={{ width: '100%' }}>
                    <Col flex="auto">
                        <Form.Item name="chat">
                            <Input placeholder="Nhập nội dung tin nhắn" />
                        </Form.Item>
                    </Col>
                    <Col>
                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                Gửi
                            </Button>
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </div>
    );
};

export default ChatPopup;
