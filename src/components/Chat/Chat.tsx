import React, { FC, Fragment, useState, useEffect } from 'react';
import classes from './styles.module.scss';
import GET_ALL_ROOM_QUERY from '@/queries/queries/GetAllRoom.graphql';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { Spin, Avatar, Row, Col, Button } from 'antd';
import ChatPopup from './ChatPopup';
import { useProfile } from '@/containers/profiles/useProfile';

const AVATAR_SIZE: number = 75;

const Chat: FC = () => {
    const { data, loading, error, refetch } = useQuery(GET_ALL_ROOM_QUERY, {
        fetchPolicy: 'network-only',
    });
    let content;
    let account_name;
    const [room, setRoom] = useState({ room_id: '' });
    const hookProps = useProfile({});
    const { profileData, isLoading } = hookProps;

    const AvatarComponent: FC<any> = ({ data }): JSX.Element => {
        if (data) {
            const { avatarUrl, account_name } = data;
            if (!avatarUrl)
                return (
                    <Avatar size={AVATAR_SIZE}>
                        <p className={classes.avatar_text}>
                            {account_name[0].toUpperCase()}
                        </p>
                    </Avatar>
                );
            return <Avatar src={data && data.avatarUrl} size={AVATAR_SIZE} />;
        }
    };

    useEffect(() => {
        if (data) setRoom(data.getAllRoom[0]);
    }, [data]);

    if (loading || isLoading) return <Spin></Spin>;
    if (error) return <div>Data fetch error</div>;
    if (data && profileData) {
        content = data.getAllRoom;
        account_name = profileData.account_name;
    }

    return (
        <div className={classes.root}>
            <h1 className={classes.title}>Trò chuyện</h1>
            <Button onClick={() => {}}>Tạo cuộc trò chuyện mới</Button>
            <Row
                className={classes.main}
                gutter={{ xs: 8, sm: 16, md: 16, lg: 16 }}
            >
                <Col span={24} md={2}>
                    <div className={classes.list_room}>
                        {content?.map((_room, index) => {
                            console.log(_room);
                            const { users } = _room;
                            let usersWithoutCurrent;
                            if (users.length > 1) {
                                usersWithoutCurrent = users.filter(
                                    user => user.account_name !== account_name
                                );
                            } else {
                                usersWithoutCurrent = users;
                            }
                            return (
                                <Fragment>
                                    <div
                                        className={`${classes.room_item} ${
                                            // @ts-ignore: Object is possibly 'null'.
                                            room.room_id === _room.room_id
                                                ? classes.active
                                                : ''
                                        }`}
                                        key={index}
                                        onClick={() => {
                                            setRoom(_room);
                                        }}
                                    >
                                        <AvatarComponent
                                            data={usersWithoutCurrent[0]}
                                        />
                                        <p className={classes.avatar_name}>
                                            {
                                                usersWithoutCurrent[0]
                                                    .account_name
                                            }
                                        </p>
                                    </div>
                                </Fragment>
                            );
                        })}
                    </div>
                </Col>
                <Col span={24} md={22}>
                    <div className={classes.chat_main}>
                        {room && <ChatPopup data={room} />}
                    </div>
                </Col>
            </Row>
        </div>
    );
};

export default Chat;
