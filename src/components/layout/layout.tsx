import React, { FC } from 'react';
import classes from './styles/layout.module.scss';
import { Layout as LayoutContainer } from 'antd';
import Menu from '../menu';
import Header from '../common/header';
import { useMediaQuery } from 'react-responsive';

const {
    Header: HeaderContainer,
    Content: ContentContainer,
    Sider: SiderContainer,
} = LayoutContainer;

const Layout: FC = ({ children }) => {
    const isDesktopOrLaptop = useMediaQuery({
        query: '(min-width: 768px)',
    });
    console.log(isDesktopOrLaptop);
    if (isDesktopOrLaptop === true || isDesktopOrLaptop === false)
        return (
            <div className={classes.root}>
                <LayoutContainer>
                    <SiderContainer
                        breakpoint="md"
                        collapsedWidth="0"
                        className={classes.sider}
                        onBreakpoint={broken => {
                            console.log(broken);
                        }}
                        onCollapse={(collapsed, type) => {
                            console.log(collapsed, type);
                        }}
                    >
                        <Menu />
                    </SiderContainer>
                    <LayoutContainer>
                        <HeaderContainer>
                            <Header />
                        </HeaderContainer>
                        <LayoutContainer className={classes.layout_container}>
                            <ContentContainer>{children}</ContentContainer>
                        </LayoutContainer>
                    </LayoutContainer>
                </LayoutContainer>
            </div>
        );
};

export default Layout;
