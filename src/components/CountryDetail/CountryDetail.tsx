import React, { FC } from 'react';
import classes from './styles.module.scss';
import { useRouter } from 'next/router';
import { useQuery } from '@apollo/react-hooks';
import GET_SONG_BY_COUNTRY_ID from '@/queries/queries/GetSongByCountryId.graphql';
import GET_COUNTRY_BY_COUNTRY_ID from '@/queries/queries/GetCountryByCountryId.graphql';
import Song from '@/components/Song';
import { Row, Col, Spin, Empty } from 'antd';
import LoadingIndicator from '@/components/base/loadingIndicator';

const CountryDetail: FC = () => {
    const router = useRouter();
    let { id: QueryId } = router.query;
    const id = typeof QueryId === 'string' ? QueryId : '';
    const {
        data: songs,
        loading: songsLoading,
        error: songsError,
        refetch: songRefetch,
    } = useQuery(GET_SONG_BY_COUNTRY_ID, {
        fetchPolicy: 'network-only',
        variables: {
            country_id: parseInt(id),
        },
    });

    const {
        data: country,
        loading: countryLoading,
        error: countryError,
    } = useQuery(GET_COUNTRY_BY_COUNTRY_ID, {
        fetchPolicy: 'network-only',
        variables: {
            country_id: parseInt(id),
        },
    });

    const refetch = () => {
        songRefetch();
    };

    if (songsLoading || countryLoading) {
        return <LoadingIndicator loadingTitle="Đang tải dữ liệu" />;
    }

    if (songsError || countryError) {
        return <div>Fetch song error...</div>;
    }

    if (!songs || !country) {
        return <div>Fetch song error...</div>;
    }

    const data = songs.getSongByCountryId;
    const dataCountry = country.getCountryByCountryId.name;

    console.log('[data]', data);

    return (
        <div className={classes.root}>
            <h1 className={classes.title}>Quốc gia: {dataCountry}</h1>
            {data.length > 0 ? (
                <Row
                    gutter={[
                        { xs: 8, sm: 16, md: 24, lg: 32 },
                        { xs: 8, sm: 16, md: 24, lg: 32 },
                    ]}
                >
                    {data.map((song, index) => {
                        return (
                            <Col xs={24} sm={24} md={12} lg={8} xl={6}>
                                <Song data={song} refetch={refetch} />
                            </Col>
                        );
                    })}
                </Row>
            ) : (
                <div className={classes.empty}>
                    <Empty
                        image="/static/images/song-placeholder.png"
                        imageStyle={{
                            height: 160,
                        }}
                        description={
                            <h1 className={classes.empty_text}>
                                Không tìm thấy bất cứ kết quả nào phù hợp
                            </h1>
                        }
                    ></Empty>
                </div>
            )}
        </div>
    );
};

export default CountryDetail;
