import React, { FC } from 'react';
import { Form, Input } from 'antd';
import classes from './styles.module.scss';
import AccountWrapper from '../common/accountWrapper';
import Link from 'next/link';
import * as routes from '../../constants/routes/routes';
import Button from '../base/button';
import { useForgotPassword } from '../../containers/ForgotPassword/useForgotPassword';
import LoadingIndicator from '../base/loadingIndicator';
import { useRouter } from 'next/router';

const layout: Array<number> = [24, 24, 6, 6, 5, 4];
const { Item } = Form;

const ForgotPassword: FC = () => {
    const router = useRouter();
    const afterSubmit = () => {
        router.push(routes.RESET_PASSWORD);
    };
    const hookProps = useForgotPassword({ afterSubmit });

    const {
        handleConfirmToken,
        confirmError,
        confirmLoading,
        timesLeft,
        zeroTimeError,
    } = hookProps;

    if (confirmLoading) {
        return <LoadingIndicator loadingTitle={'Authenticating token...'} />;
    }

    return (
        <AccountWrapper layout={layout}>
            <div className={classes.root}>
                <div className={classes.form_container}>
                    <Link href={routes.HOME}>
                        <img
                            src={'/static/images/logo-alt.png'}
                            className={classes.logo}
                        />
                    </Link>
                    <Form
                        className={classes.form}
                        layout="vertical"
                        onFinish={handleConfirmToken}
                    >
                        <Item
                            label={
                                <span className={classes.form__label}>
                                    Nhập mã xác thực
                                </span>
                            }
                            name="token"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập mã xác thực',
                                },
                            ]}
                        >
                            <div className={classes.form__input_container}>
                                <Input
                                    id="confirm_token_input"
                                    size="large"
                                    placeholder="Mã xác thực"
                                    bordered={false}
                                    autoComplete="off"
                                />
                            </div>
                        </Item>
                        <Item className={classes.form__button_container}>
                            <Button
                                type="primary"
                                htmlType="submit"
                                size="large"
                                className={classes.form__button}
                            >
                                Xác nhận
                            </Button>
                        </Item>
                    </Form>
                    <span>
                        {confirmError &&
                            !zeroTimeError &&
                            `${confirmError.message}, bạn còn ${timesLeft} lần nhập`}
                    </span>
                    <span>
                        {zeroTimeError && (
                            <>
                                Bạn đã nhập quá số lần quy định, vui lòng gửi
                                lại email
                                <Link href={routes.FORGOT_PASSWORD}>
                                    Click vào đây để gửi lại email
                                </Link>
                            </>
                        )}
                    </span>
                </div>
            </div>
        </AccountWrapper>
    );
};

export default ForgotPassword;
