import React, { FC } from 'react';
import { Form, Input } from 'antd';
import classes from './styles.module.scss';
import AccountWrapper from '../common/accountWrapper';
import Link from 'next/link';
import * as routes from '../../constants/routes/routes';
import Button from '../base/button';
import { useForgotPassword } from '../../containers/ForgotPassword/useForgotPassword';
import LoadingIndicator from '../base/loadingIndicator';
import { useRouter } from 'next/router';

const layout: Array<number> = [24, 24, 6, 6, 5, 4];
const { Item } = Form;

const ResetPassword: FC = () => {
    const router = useRouter();
    const afterSubmit = () => {
        router.push(routes.LOGIN);
    };
    const hookProps = useForgotPassword({ afterSubmit });

    const { handleResetPassword, resetLoading, resetError } = hookProps;

    if (resetLoading) {
        return <LoadingIndicator loadingTitle={'Authenticating token...'} />;
    }

    return (
        <AccountWrapper layout={layout}>
            <div className={classes.root}>
                <div className={classes.form_container}>
                    <Link href={routes.HOME}>
                        <img
                            src={'/static/images/logo-alt.png'}
                            className={classes.logo}
                        />
                    </Link>
                    <Form
                        className={classes.form}
                        layout="vertical"
                        onFinish={handleResetPassword}
                    >
                        <Item
                            label={
                                <span className={classes.form__label}>
                                    Nhập mật khẩu mới
                                </span>
                            }
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập mật khẩu mới',
                                },
                            ]}
                        >
                            <div className={classes.form__input_container}>
                                <Input.Password
                                    id="reset_password_input"
                                    size="large"
                                    placeholder="Mật khẩu mới"
                                    bordered={false}
                                    autoComplete="off"
                                />
                            </div>
                        </Item>
                        <Item
                            label={
                                <span className={classes.form__label}>
                                    Nhập mật khẩu mới
                                </span>
                            }
                            name="cofirm_password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng xác nhận mật khẩu mới',
                                },
                                ({ getFieldValue }) => ({
                                    validator(_, value) {
                                        if (
                                            !value ||
                                            getFieldValue('password') === value
                                        ) {
                                            return Promise.resolve();
                                        }
                                        return Promise.reject(
                                            new Error(
                                                'Mật khẩu bạn đã nhập không khớp'
                                            )
                                        );
                                    },
                                }),
                            ]}
                        >
                            <div className={classes.form__input_container}>
                                <Input.Password
                                    id="reset_password_input"
                                    size="large"
                                    placeholder="Mật khẩu mới"
                                    bordered={false}
                                    autoComplete="off"
                                />
                            </div>
                        </Item>
                        <Item className={classes.form__button_container}>
                            <Button
                                type="primary"
                                htmlType="submit"
                                size="large"
                                className={classes.form__button}
                            >
                                Thay đổi
                            </Button>
                        </Item>
                    </Form>
                    <span>{resetError?.message}</span>
                </div>
            </div>
        </AccountWrapper>
    );
};

export default ResetPassword;
