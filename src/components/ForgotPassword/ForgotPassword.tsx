import React, { FC } from 'react';
import { Form, Input } from 'antd';
import classes from './styles.module.scss';
import AccountWrapper from '../common/accountWrapper';
import Link from 'next/link';
import * as routes from '../../constants/routes/routes';
import Button from '../base/button';
import { useForgotPassword } from '../../containers/ForgotPassword/useForgotPassword';
import LoadingIndicator from '../base/loadingIndicator';
import { useRouter } from 'next/router';

const layout: Array<number> = [24, 24, 6, 6, 5, 4];
const { Item } = Form;

const ForgotPassword: FC = () => {
    const router = useRouter();
    const afterSubmit = () => {
        router.push(routes.CONFIRM_TOKEN);
    };
    const hookProps = useForgotPassword({ afterSubmit });

    const { loading, error, handleSubmit } = hookProps;

    if (loading) {
        return <LoadingIndicator loadingTitle={'Sending mail...'} />;
    }

    return (
        <AccountWrapper layout={layout}>
            <div className={classes.root}>
                <div className={classes.form_container}>
                    <Link href={routes.HOME}>
                        <img
                            src={'/static/images/logo-alt.png'}
                            className={classes.logo}
                        />
                    </Link>
                    <Form
                        className={classes.form}
                        layout="vertical"
                        onFinish={handleSubmit}
                    >
                        <Item
                            label={
                                <span className={classes.form__label}>
                                    Email
                                </span>
                            }
                            name="email"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập email của bạn',
                                },
                            ]}
                        >
                            <div className={classes.form__input_container}>
                                <Input
                                    id="forgot_email_input"
                                    size="large"
                                    placeholder="Email"
                                    bordered={false}
                                    autoComplete="off"
                                />
                            </div>
                        </Item>
                        <Item className={classes.form__button_container}>
                            <Button
                                type="primary"
                                htmlType="submit"
                                size="large"
                                className={classes.form__button}
                            >
                                Gửi mã xác thực
                            </Button>
                        </Item>
                    </Form>
                    <span>{error?.message}</span>
                </div>
            </div>
        </AccountWrapper>
    );
};

export default ForgotPassword;
