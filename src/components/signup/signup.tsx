import React, { FC, useState } from 'react';
import { useRouter } from 'next/router';
import classes from './styles/signup.module.scss';
import {
    Form,
    Button,
    Input,
    Modal,
    Spin,
    Alert,
    Row,
    Col,
    Radio,
    DatePicker,
} from 'antd';
import PropTypes from 'prop-types';
import { useSignUp } from '../../containers/users/useSignUp';
import Icon, {
    UserOutlined,
    EyeInvisibleOutlined,
    EyeTwoTone,
    LockOutlined,
} from '@ant-design/icons';
import { checkSignedIn } from '../../utils/checkSignIn';
import Link from 'next/link';
import AccountWrapper from '../common/accountWrapper';
import * as routes from '../../constants/routes/routes';
import UsersSvg from './assets/icons/users.svg';
import EmailSvg from './assets/icons/envelope.svg';
import PasswordSvg from './assets/icons/unlock-alt.svg';
import VisibleSvg from './assets/icons/eye.svg';
import InvisibleSvg from './assets/icons/eye-slash.svg';

interface SignUpProps {
    title: string;
}

const formLayout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

const dateFormat: string = 'DD/MM/YYYY';

const SignUp: FC<SignUpProps> = ({ title }: SignUpProps) => {
    const router = useRouter();
    const [form] = Form.useForm();
    const [isVisibleModal, setVisibleModal] = useState<boolean | undefined>(
        false
    );

    const handleCloseModal: VoidFunction = (): void => {
        setVisibleModal(false);
    };
    const afterSubmit: VoidFunction = (): void => {
        setVisibleModal(true);
    };

    const handleRedirectToLogin: VoidFunction = (): void => {
        router.push('login');
    };

    const hookProps = useSignUp({ afterSubmit });
    const { errors, createAccountLoading, handleCreateAccount } = hookProps;

    if (createAccountLoading) {
        return (
            <div className={classes.loading_indicator}>
                <Spin tip="Đang tạo tài khoản..." size="large"></Spin>
            </div>
        );
    }
    const errorMessageList: React.ReactElement =
        errors.length > 0 ? (
            <ul className={classes.error_list}>
                {errors.map((error, index) => {
                    return <li key={index}>{error}</li>;
                })}
            </ul>
        ) : null;

    const layout: Array<number> = [48, 48, 18, 18, 15, 12, 12];

    return (
        <AccountWrapper layout={layout}>
            <div className={classes.root}>
                <div className={classes.form_container}>
                    <Link href={routes.HOME}>
                        <img
                            src={'/static/images/logo-alt.png'}
                            className={classes.form_container__logo}
                        />
                    </Link>
                    <Modal
                        title="Chúc mừng"
                        visible={isVisibleModal}
                        onOk={handleCloseModal}
                        footer={[
                            <Button key="back" onClick={handleRedirectToLogin}>
                                Đăng nhập
                            </Button>,
                            <Button
                                key="submit"
                                type="primary"
                                onClick={handleCloseModal}
                            >
                                Ok
                            </Button>,
                        ]}
                    >
                        <p>Tạo tài khoản thành công</p>
                    </Modal>
                    <Form
                        className={classes.login_form}
                        onFinish={handleCreateAccount}
                        layout={'vertical'}
                        form={form}
                    >
                        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                            <Col md={24} lg={12}>
                                <Form.Item
                                    label={
                                        <span className={classes.label}>
                                            {'Tên tài khoản'}
                                        </span>
                                    }
                                    name="account_name"
                                    hasFeedback
                                    rules={[
                                        {
                                            required: true,
                                            message:
                                                'Vui lòng nhập tên tài khoản của bạn',
                                        },
                                        {
                                            pattern: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,12}$/,
                                            message:
                                                'Tên tài khoản phải có ít nhất 4 ký tự, tối đa 12 ký tự, không bao gồm ký tự đặt biệt',
                                        },
                                    ]}
                                >
                                    <div className={classes.input_container}>
                                        <Input
                                            id={'signup_user_input'}
                                            size="large"
                                            placeholder="Tên tài khoản"
                                            prefix={
                                                <Icon
                                                    component={UsersSvg}
                                                    style={{
                                                        marginRight:
                                                            '0.375rem!important',
                                                        color: '#fe0068',
                                                    }}
                                                />
                                            }
                                            bordered={false}
                                            className={
                                                classes.account_name_input
                                            }
                                        />
                                    </div>
                                </Form.Item>
                                <Form.Item
                                    label={
                                        <span className={classes.label}>
                                            {'Mật khẩu'}
                                        </span>
                                    }
                                    name="password"
                                    hasFeedback
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập mật khẩu',
                                        },
                                        {
                                            pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/,
                                            message:
                                                'Mật khẩu phải có ít nhất 8 ký tự, bao gồm chữ hoa, chữ thường và số, không bao gồm ký tự đặc biệt',
                                        },
                                    ]}
                                >
                                    <div className={classes.input_container}>
                                        <Input.Password
                                            id={'signup_password_input'}
                                            size="large"
                                            placeholder="Mật khẩu"
                                            iconRender={visible =>
                                                visible ? (
                                                    <Icon
                                                        component={VisibleSvg}
                                                        style={{
                                                            color: '#333333',
                                                        }}
                                                    />
                                                ) : (
                                                    <Icon
                                                        component={InvisibleSvg}
                                                        style={{
                                                            color: '#333333',
                                                        }}
                                                    />
                                                )
                                            }
                                            prefix={
                                                <Icon
                                                    component={PasswordSvg}
                                                    style={{
                                                        marginRight:
                                                            '0.375rem!important',
                                                        color: '#fe0068',
                                                    }}
                                                />
                                            }
                                            bordered={false}
                                            className={classes.password_input}
                                        />
                                    </div>
                                </Form.Item>
                                <Form.Item
                                    label={
                                        <span className={classes.label}>
                                            {'Xác nhận mật khẩu'}
                                        </span>
                                    }
                                    name="confirm_password"
                                    hasFeedback
                                    dependencies={['password']}
                                    rules={[
                                        {
                                            required: true,
                                            message:
                                                'Vui lòng nhập lại mật khẩu để xác nhận',
                                        },
                                        {
                                            pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/,
                                            message:
                                                'Mật khẩu phải có ít nhất 8 ký tự, bao gồm chữ hoa, chữ thường và số, không bao gồm ký tự đặc biệt',
                                        },
                                        ({ getFieldValue }) => ({
                                            validator(rule, value) {
                                                if (
                                                    !value ||
                                                    getFieldValue(
                                                        'password'
                                                    ) === value
                                                ) {
                                                    return Promise.resolve();
                                                }
                                                return Promise.reject(
                                                    'Xác nhận mật khẩu không khớp'
                                                );
                                            },
                                        }),
                                    ]}
                                >
                                    <div className={classes.input_container}>
                                        <Input.Password
                                            id={'signup_password_confirm_input'}
                                            size="large"
                                            placeholder="Xác nhận mật khẩu"
                                            iconRender={visible =>
                                                visible ? (
                                                    <Icon
                                                        component={VisibleSvg}
                                                        style={{
                                                            color: '#333333',
                                                        }}
                                                    />
                                                ) : (
                                                    <Icon
                                                        component={InvisibleSvg}
                                                        style={{
                                                            color: '#333333',
                                                        }}
                                                    />
                                                )
                                            }
                                            prefix={
                                                <Icon
                                                    component={PasswordSvg}
                                                    style={{
                                                        marginRight:
                                                            '0.375rem!important',
                                                        color: '#fe0068',
                                                    }}
                                                />
                                            }
                                            bordered={false}
                                            //name="password"
                                            className={classes.password_input}
                                        />
                                    </div>
                                </Form.Item>
                            </Col>
                            <Col md={24} lg={12}>
                                <Form.Item
                                    label={
                                        <span className={classes.label}>
                                            {'Email'}
                                        </span>
                                    }
                                    name="email"
                                    hasFeedback
                                    rules={[
                                        {
                                            required: true,
                                            message:
                                                'Vui lòng nhập email của bạn',
                                        },
                                        {
                                            pattern: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/,
                                            message:
                                                'Email của bạn không hợp lệ',
                                        },
                                    ]}
                                >
                                    <div className={classes.input_container}>
                                        <Input
                                            id={'signup_email_input'}
                                            size="large"
                                            placeholder="Email"
                                            prefix={
                                                <Icon
                                                    component={EmailSvg}
                                                    style={{
                                                        marginRight:
                                                            '0.375rem!important',
                                                        color: '#fe0068',
                                                    }}
                                                />
                                            }
                                            bordered={false}
                                            className={
                                                classes.account_name_input
                                            }
                                        />
                                    </div>
                                </Form.Item>
                                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                    <Col span={12}>
                                        <Form.Item
                                            label={
                                                <span className={classes.label}>
                                                    {'Họ và chữ lót'}
                                                </span>
                                            }
                                            name="first_name"
                                            hasFeedback
                                            rules={[
                                                {
                                                    required: true,
                                                    message:
                                                        'Vui lòng nhập họ và chữ lót của bạn',
                                                },
                                            ]}
                                        >
                                            <div
                                                className={
                                                    classes.input_container
                                                }
                                            >
                                                <Input
                                                    id={'signup_f_name_input'}
                                                    size="large"
                                                    placeholder="Họ và chữ lót"
                                                    prefix={
                                                        <Icon
                                                            component={UsersSvg}
                                                            style={{
                                                                marginRight:
                                                                    '0.375rem!important',
                                                                color:
                                                                    '#fe0068',
                                                            }}
                                                        />
                                                    }
                                                    bordered={false}
                                                    className={
                                                        classes.password_input
                                                    }
                                                />
                                            </div>
                                        </Form.Item>
                                    </Col>
                                    <Col span={12}>
                                        <Form.Item
                                            label={
                                                <span className={classes.label}>
                                                    {'Tên'}
                                                </span>
                                            }
                                            name="last_name"
                                            hasFeedback
                                            rules={[
                                                {
                                                    required: true,
                                                    message:
                                                        'Vui lòng nhập tên của bạn',
                                                },
                                            ]}
                                        >
                                            <div
                                                className={
                                                    classes.input_container
                                                }
                                            >
                                                <Input
                                                    id={'signup_l_name_input'}
                                                    size="large"
                                                    placeholder="Tên"
                                                    prefix={
                                                        <Icon
                                                            component={UsersSvg}
                                                            style={{
                                                                marginRight:
                                                                    '0.375rem!important',
                                                                color:
                                                                    '#fe0068',
                                                            }}
                                                        />
                                                    }
                                                    bordered={false}
                                                    className={
                                                        classes.password_input
                                                    }
                                                />
                                            </div>
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                                    <Col span={12}>
                                        <Form.Item
                                            label={
                                                <span className={classes.label}>
                                                    {'Giới tính'}
                                                </span>
                                            }
                                            name="gender"
                                            rules={[
                                                {
                                                    required: true,
                                                    message:
                                                        'Vui lòng chọn giới tính',
                                                },
                                            ]}
                                        >
                                            <Radio.Group
                                                style={{
                                                    marginTop: '0.5rem',
                                                    display: 'flex',
                                                    justifyContent:
                                                        'space-around',
                                                }}
                                            >
                                                <Radio value={true}>
                                                    <span
                                                        className={
                                                            classes.gender_text
                                                        }
                                                    >
                                                        Nam
                                                    </span>
                                                </Radio>
                                                <Radio value={false}>
                                                    <span
                                                        className={
                                                            classes.gender_text
                                                        }
                                                    >
                                                        Nữ
                                                    </span>
                                                </Radio>
                                            </Radio.Group>
                                        </Form.Item>
                                    </Col>
                                    <Col span={12}>
                                        <Form.Item
                                            name="birthday"
                                            label={
                                                <span className={classes.label}>
                                                    Sinh nhật
                                                </span>
                                            }
                                            hasFeedback
                                        >
                                            <div
                                                className={
                                                    classes.input_container
                                                }
                                            >
                                                <DatePicker
                                                    id="signup_birthday_input"
                                                    format={dateFormat}
                                                    bordered={false}
                                                    onChange={value => {
                                                        form.setFieldsValue({
                                                            birthday: value,
                                                        });
                                                        console.log(value);
                                                    }}
                                                />
                                            </div>
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>

                        <div className={classes.action_buttons}>
                            <Form.Item
                                style={{
                                    width: '50%',
                                    marginBottom: '0.75rem',
                                    marginTop: '0.5rem',
                                }}
                            >
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    size={'large'}
                                    className={classes.action_buttons__signup}
                                >
                                    Đăng ký
                                </Button>
                            </Form.Item>
                            <Form.Item
                                style={{
                                    marginBottom: '0rem',
                                }}
                            >
                                <Link href="login">
                                    <Button
                                        type="link"
                                        className={
                                            classes.action_buttons__login
                                        }
                                    >
                                        Đã có tài khoản
                                    </Button>
                                </Link>
                            </Form.Item>
                        </div>
                    </Form>
                    {errorMessageList}
                </div>
            </div>
        </AccountWrapper>
    );
};

SignUp.defaultProps = {
    title: '',
};

SignUp.propTypes = {
    title: PropTypes.string.isRequired,
};

export default SignUp;
