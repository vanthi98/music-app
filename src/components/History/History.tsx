import React, { FC, useEffect } from 'react';
import classes from './styles.module.scss';
import { useQuery } from '@apollo/react-hooks';
import LoadingIndicator from '@/components/base/loadingIndicator';
import SongItem from '@/components/UploadSong/Item';
import GET_HISTORY_SONG_BY_CURRENT_ACCOUNT from '@/queries/GetHistoryByCurrentAccount.graphql';

const HistorySong: FC<any> = () => {
    const { data: songs, loading, error, refetch } = useQuery(
        GET_HISTORY_SONG_BY_CURRENT_ACCOUNT,
        {
            fetchPolicy: 'network-only',
        }
    );

    useEffect(() => {
        refetch();
    }, []);

    if (loading) return <LoadingIndicator loadingTitle="loading history" />;
    if (error)
        return <div className={classes.error_page}>Error when fetch data</div>;
    if (!songs) {
        return <div className={classes.error_page}>Error when fetch data</div>;
    }

    let list: JSX.Element;

    if (songs) {
        list = (
            <div className={classes.content__list} id="list">
                <h1 className={classes.title}>Lịch sử các bài đã nghe</h1>
                {songs.getHistoryByCurrentAccount.map(song => {
                    return (
                        <SongItem
                            data={song}
                            edit="disable"
                            refetch={refetch}
                        ></SongItem>
                    );
                })}
            </div>
        );
    }

    return <div className={classes.root}>{list}</div>;
};

export default HistorySong;
