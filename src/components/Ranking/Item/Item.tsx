import React, { FC } from 'react';
import classes from './styles.module.scss';

interface ISong {
    _id: string;
    playlist_id?: string;
    song_name: string;
    author?: string;
    song_url: string;
    song_image_url?: string;
    lyric?: string;
    like: number;
    comment: number;
    listen: number;
    uploader: string;
    duration?: number;
    listLikedUser?: Array<string>;
    listComment?: Array<string>;
    createdAt?: Date;
}

interface IItemProps {
    data: ISong;
}

const Item: FC<IItemProps> = ({ data }) => {
    return (
        <div className={classes.root}>
            <h1>{data.song_name}</h1>
        </div>
    );
};

export default Item;
