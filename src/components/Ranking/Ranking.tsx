import React, { FC, useState } from 'react';
import classes from './styles.module.scss';
import { useQuery } from '@apollo/react-hooks';
import GET_TRENDING_SONG from '@/queries/queries/GetTrendingSong.graphql';
import { Spin, Row, Col, Empty, Button } from 'antd';
import SongItem from '@/components/UploadSong/Item';

interface ISong {
    _id: string;
    playlist_id?: string;
    song_name: string;
    author?: string;
    song_url: string;
    song_image_url?: string;
    lyric?: string;
    like: number;
    comment: number;
    listen: number;
    uploader: string;
    duration?: number;
    listLikedUser?: Array<string>;
    listComment?: Array<string>;
    createdAt?: Date;
}

const Ranking: FC = () => {
    const { data: songs, loading: songsLoading, error: songsError } = useQuery(
        GET_TRENDING_SONG,
        {
            fetchPolicy: 'network-only',
        }
    );
    const [count, setCount] = useState<number | undefined>(10);

    if (songsLoading) {
        return <Spin tip="Get song..."></Spin>;
    }

    if (songsError) {
        return <div>Fetch song error...</div>;
    }

    if (!songs) {
        return <div>Fetch song error...</div>;
    }

    const data = songs.getTrendingSong;

    return (
        <div className={classes.root}>
            <h1 className={classes.title}>Bảng xếp hạng bài hát</h1>
            {data.length > 0 ? (
                data.slice(0, count).map((song: ISong, index) => {
                    return (
                        <Row
                            align="middle"
                            gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}
                            justify="space-between"
                            className={classes.item}
                        >
                            <Col flex="100px">
                                <div
                                    className={`${classes.rank} ${
                                        index === 0 && classes.first
                                    } ${index === 1 && classes.second} ${
                                        index === 2 && classes.third
                                    }`}
                                >
                                    {index + 1}
                                </div>
                            </Col>
                            <Col flex="auto">
                                <SongItem
                                    data={song}
                                    key={index}
                                    edit="disable"
                                />
                            </Col>
                        </Row>
                    );
                })
            ) : (
                <div className={classes.empty}>
                    <Empty
                        image="/static/images/song-placeholder.png"
                        imageStyle={{
                            height: 160,
                        }}
                        description={
                            <h1 className={classes.empty_text}>
                                Không tìm thấy bất cứ kết quả nào phù hợp
                            </h1>
                        }
                    ></Empty>
                </div>
            )}
            {data.length > 0 && count === 10 && (
                <Button
                    onClick={() => setCount(100)}
                    size="large"
                    type="primary"
                    className={classes.loadmore}
                >
                    Xem top 100
                </Button>
            )}
        </div>
    );
};

export default Ranking;
