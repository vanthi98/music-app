import React, { FC, useState } from 'react';
import classes from './styles/test.module.scss';
import { Button } from 'antd';
import { useQuery, gql } from '@apollo/client';
import USER_QUERY from '../../queries/getUser.graphql';
import CREATE_USER_MUTATION from '../../queries/addUser.graphql';
import PropTypes from 'prop-types';
import { useUser } from '../../containers/test/useUser';
interface TestProps {
    title: string;
}

interface User {
    name: string;
    age: number;
    gender: boolean;
}

interface UserData {
    user: User[];
}

const Test: FC<TestProps> = ({ title }: TestProps) => {
    const [formValues, setFormValues] = useState({
        account_name: '',
        password: '',
    });
    const hookProps = useUser({});
    const {
        allAccount,
        errors,
        handleLogin,
        loading,
        loginError,
        loginLoading,
        userList,
    } = hookProps;
    const onHandleChange = event => {
        const { target } = event;
        const { name, value } = target;
        setFormValues({
            ...formValues,
            [name]: value,
        });
    };

    if (loading) {
        return <p>Loading...</p>;
    }
    if (errors.length > 0) {
        return <p>Data fetch error...</p>;
    }
    return (
        <>
            <h1 className={classes.title}>{title}</h1>
            <ul>
                {userList &&
                    userList.user.map((user: User, index) => {
                        return <li key={index}>{user.name}</li>;
                    })}
            </ul>
            <ul>
                {allAccount &&
                    allAccount.map((account, index) => {
                        return <li key={index}>{account.account_name}</li>;
                    })}
            </ul>
            <form>
                <input
                    type="text"
                    name="account_name"
                    onChange={onHandleChange}
                />
                <input type="text" name="password" onChange={onHandleChange} />
                <Button type="primary" onClick={() => handleLogin(formValues)}>
                    Click me
                </Button>
            </form>
        </>
    );
};

Test.defaultProps = {
    title: '',
};

Test.propTypes = {
    title: PropTypes.string.isRequired,
};

export default Test;
