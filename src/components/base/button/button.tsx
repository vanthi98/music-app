import React, { FC, useState } from 'react';
import classes from './styles/button.module.scss';
import { string } from 'prop-types';
import { Button } from 'antd';
import { useSpring, animated } from 'react-spring';

const AnimatedButton = animated(Button);

const CustomButton: FC<any> = (props: any) => {
    const { style } = props;
    const [animate, toggle] = useState(false);
    const animatedProps = useSpring({
        from: { x: 0 },
        x: animate ? 1 : 0,
        config: { duration: 200 },
    });
    return (
        <AnimatedButton
            className={classes.root}
            onClick={() => toggle(!animate)}
            style={{
                ...style,
                transform: animatedProps.x
                    .interpolate({
                        range: [0, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 1],
                        output: [1, 1.97, 1.9, 2.1, 1.9, 2.1, 1.03, 1],
                    })
                    .interpolate(x => `scale(${x})`),
            }}
            {...props}
        >
            {props.children}
        </AnimatedButton>
    );
};

CustomButton.propTypes = {
    type: string,
};

CustomButton.defaultProps = {
    type: 'primary',
};

export default CustomButton;
