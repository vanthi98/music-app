import React, { FC } from 'react';
import classes from './styles/loadingIndicator.module.scss';
import { Spin } from 'antd';
import PropTypes, { string } from 'prop-types';

interface LoadingIndicatorProps {
    loadingTitle: string;
}

const LoadingIndicator: FC<LoadingIndicatorProps> = ({
    loadingTitle,
}: LoadingIndicatorProps) => {
    return (
        <div className={classes.root}>
            <div className={classes.content}>
                <Spin size={'large'}></Spin>
                <p className={classes.title}>{loadingTitle}</p>
            </div>
        </div>
    );
};

LoadingIndicator.propTypes = {
    loadingTitle: string.isRequired,
};

LoadingIndicator.defaultProps = {
    loadingTitle: 'Loading...',
};

export default LoadingIndicator;
