import React, { FC, useCallback } from 'react';
import classes from './styles.module.scss';
import Icon from '@ant-design/icons';
import PlusSvg from '@public/static/icons/plus.svg';
import { useMutation } from '@apollo/react-hooks';
import ADD_SONG_TO_PLAYLIST from '@/queries/AddSongToPlaylist.graphql';
import { notification } from 'antd';

const PlaylistItem: FC<{ data: any; song_id: string }> = ({
    data,
    song_id,
}) => {
    const [addSongToPlaylistMutation, { loading, error }] = useMutation(
        ADD_SONG_TO_PLAYLIST
    );
    const handleAddSongToPlaylist = useCallback(async () => {
        try {
            await addSongToPlaylistMutation({
                variables: {
                    playlist_id: data._id,
                    song_id,
                },
            });
            notification.success({
                message: `Thêm bài hát vào playlist thành công`,
            });
        } catch (error) {
            notification.error({
                message: error.message,
            });
        }
    }, [addSongToPlaylistMutation, data]);

    return (
        <div className={classes.root} onClick={handleAddSongToPlaylist}>
            {data.name}
            <Icon component={PlusSvg} />
        </div>
    );
};

export default PlaylistItem;
