import React, { FC, useState, useCallback } from 'react';
import classes from './styles.module.scss';
import { Button, Modal, Form, Input, notification } from 'antd';
import Icon from '@ant-design/icons';
import PlusSvg from '@public/static/icons/plus.svg';
import GET_ALL_PLAYLIST from '@/queries/GetAllPlaylist.graphql';
import CREATE_PLAYLIST_MUTATION from '@/queries/CreatePlaylist.graphql';
import { useQuery, useMutation } from '@apollo/react-hooks';
import LoadingIndicator from '@/components/base/loadingIndicator';
import PlaylistItem from './PlaylistItem';

const AddToPlaylist: FC<any> = ({ song_id }) => {
    const { data: playlists, error, loading, refetch } = useQuery(
        GET_ALL_PLAYLIST,
        {
            fetchPolicy: 'network-only',
        }
    );

    const [isModalVisible, setIsModalVisible] = useState<boolean | undefined>(
        false
    );
    const [form] = Form.useForm();
    const [createPlaylistMutation] = useMutation(CREATE_PLAYLIST_MUTATION);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleCreatePlaylist = useCallback(
        async formValues => {
            const { playlist_name } = formValues;
            await createPlaylistMutation({
                variables: {
                    name: playlist_name,
                    listSong: [song_id],
                },
            });
            if (refetch) {
                refetch();
            }
            setIsModalVisible(false);
            form.resetFields();
            notification.success({
                message: `Tạo playlist '${playlist_name}' thành công`,
            });
        },
        [createPlaylistMutation, setIsModalVisible, refetch]
    );

    if (loading) return <LoadingIndicator loadingTitle="loading history" />;
    if (error)
        return <div className={classes.error_page}>Error when fetch data</div>;
    if (!playlists) {
        return <div className={classes.error_page}>Error when fetch data</div>;
    }

    return (
        <div className={classes.root}>
            <Button
                icon={<Icon component={PlusSvg} />}
                style={{ marginBottom: '1rem' }}
                onClick={showModal}
            >
                Tạo mới playlist
            </Button>
            {playlists.getAllPlaylist.map((playlist, index) => {
                return (
                    <PlaylistItem
                        data={playlist}
                        key={index}
                        song_id={song_id}
                    />
                );
            })}
            <Modal
                title="Tạo playlist mới"
                visible={isModalVisible}
                onCancel={handleCancel}
                footer={[
                    <Button
                        type="primary"
                        htmlType="submit"
                        onClick={handleCancel}
                    >
                        Hủy
                    </Button>,
                ]}
            >
                <Form form={form} onFinish={handleCreatePlaylist}>
                    <Form.Item
                        name="playlist_name"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập tên playlist',
                            },
                        ]}
                    >
                        <Input placeholder="Nhập tên playlist" />
                    </Form.Item>
                    <Form.Item style={{ marginTop: '1rem' }}>
                        <Button type="primary" htmlType="submit" size={'large'}>
                            Tạo playlist
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
};

export default AddToPlaylist;
