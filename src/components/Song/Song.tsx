import React, {
    FC,
    useState,
    useEffect,
    ReactNode,
    useCallback,
    Fragment,
} from 'react';
import { useDispatch } from 'react-redux';
import classes from './styles.module.scss';
import TextEditor from '../TextEditor';
import {
    Menu,
    Dropdown,
    Modal,
    Comment,
    Tooltip,
    List,
    Form,
    Button,
    Input,
    Avatar,
    Tag,
    notification,
} from 'antd';
import Icon from '@ant-design/icons';
import ListenSvg from './listen.svg';
import HeartSvg from './heart.svg';
import CommentSvg from './comment.svg';
import ActionSvg from './action.svg';
import ShareSvg from './share.svg';
import PlaylistSvg from './playlist.svg';
import DownloadSvg from './download.svg';
import FlagSvg from './flag.svg';
import PlaySvg from './play-circle.svg';
import parseTime from '@/utils/pasreTime';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { useProfile } from '@/containers/profiles/useProfile';
import LIKE_SONG_MUTATION from '@/queries/LikeSong.graphql';
import UNLIKE_SONG_MUTATION from '@/queries/UnlikeSong.graphql';
import LISTEN_SONG from '@/queries/ListenSong.graphql';
import GET_COMMENTS_QUERY from '@/queries/GetCommentBySong.graphql';
import CREATE_COMMENT_MUTATION from '@/queries/CreateComment.graphql';
import moment from 'moment';
import AddToPlaylistModal from './AddToPlaylist';
import CalendarSvg from '@public/static/icons/calendar.svg';

moment.locale('vi');

const { Item, Divider } = Menu;
const { TextArea } = Input;

enum disableStatus {
    LIKED = 2,
    OWNER = 0,
    AVAILABLE = 1,
}

function createMarkup(html: string) {
    return { __html: html };
}

interface ISong {
    _id: string;
    playlist_id?: string;
    song_name: string;
    author?: string;
    song_url: string;
    song_image_url?: string;
    lyric?: string;
    like: number;
    comment: number;
    listen: number;
    uploader: string;
    duration?: number;
    listLikedUser?: Array<string>;
    listComment?: Array<string>;
    createdAt?: Date;
}

interface SongProps {
    data: ISong;
    refetch: Function;
}

interface IComment {
    id: string;
    parent: string;
    actions: Array<ReactNode>;
    author: string;
    avatar: string;
    content: any;
    datetime: JSX.Element;
    children: Array<string>;
}

const Song: FC<SongProps> = ({ data, refetch }): JSX.Element => {
    const dispatch = useDispatch();

    const [listenSongMutation, { error: listenError }] = useMutation(
        LISTEN_SONG
    );

    const [form] = Form.useForm();

    const [isModalVisible, setIsModalVisible] = useState<boolean | undefined>(
        false
    );

    const [replyTo, setReplyTo] = useState('');

    const [isAddModalVisible, setAddModalVisible] = useState<
        boolean | undefined
    >(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const showAddModal = () => {
        setAddModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleAddOK = () => {
        setAddModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleAddCancel = () => {
        setAddModalVisible(false);
    };

    const hookProps = useProfile({});
    const { profileId, isLoading } = hookProps;

    const {
        data: listComment,
        loading: loadingComment,
        error: errorComment,
        refetch: getCommentRefetch,
    } = useQuery(GET_COMMENTS_QUERY, {
        variables: { song_id: data._id },
        fetchPolicy: 'network-only',
    });

    const [
        createCommentMutation,
        { loading: createCommentLoading, error: createCommentError },
    ] = useMutation(CREATE_COMMENT_MUTATION);

    const handleSubmitComment = useCallback(
        async formValues => {
            try {
                const { title, content, parent, replyTo } = formValues;
                let variables = {
                    song_id: data._id,
                    title,
                    content,
                };
                if (parent && replyTo) {
                    variables['parent_id'] = parent;
                    variables['replyTo'] = replyTo;
                }
                await createCommentMutation({
                    variables,
                });

                if (getCommentRefetch) {
                    getCommentRefetch();
                }

                form.resetFields();
                setReplyTo('');
            } catch (error) {
                console.log(error);
            }
        },
        [createCommentMutation, getCommentRefetch, replyTo]
    );

    const handleReply: Function = (account_name: string, id: string): void => {
        setReplyTo(account_name);
        form.setFieldsValue({ parent: id, replyTo: account_name });
    };

    const handleRemoveReply = () => {
        setReplyTo('');
        form.setFieldsValue({ parent: '', replyTo: '' });
    };

    const commentModal: Function = (): JSX.Element => {
        if (!listComment) return null;
        const comments: Array<IComment> = listComment.getCommentBySong.map(
            comment => {
                return {
                    id: comment._id,
                    parent: comment.parent,
                    children: comment.children,
                    actions: [
                        <span
                            key="comment-list-reply-to-0"
                            onClick={() =>
                                handleReply(
                                    comment.user.account_name,
                                    comment.parent
                                        ? comment.parent
                                        : comment._id
                                )
                            }
                        >
                            Phản hồi
                        </span>,
                    ],
                    author: comment.user.account_name,
                    avatar: comment.user.avatarUrl,
                    content: (
                        <Fragment>
                            <p style={{ fontWeight: 'bold' }}>
                                {comment.title}
                            </p>
                            {comment.replyTo && (
                                <Tag color="cyan">
                                    Trả lời {`@${comment.replyTo}`}
                                </Tag>
                            )}
                            <div
                                dangerouslySetInnerHTML={createMarkup(
                                    comment.content
                                )}
                            />
                        </Fragment>
                    ),
                    datetime: (
                        <Tooltip
                            title={moment(comment.createdAt).format(
                                'YYYY-MM-DD HH:mm:ss'
                            )}
                        >
                            <span>{moment(comment.createdAt).fromNow()}</span>
                        </Tooltip>
                    ),
                };
            }
        );
        const Editor = ({ onSubmit }) => (
            <Form onFinish={onSubmit} form={form}>
                <Form.Item name="title">
                    <Input placeholder="Chủ đề" />
                </Form.Item>
                <Form.Item name="replyTo" style={{ display: 'none' }}>
                    <Input />
                </Form.Item>
                {replyTo && (
                    <Form.Item name="parent" label="Trả lời">
                        <Tag closable onClose={handleRemoveReply}>
                            {`@${replyTo}`}
                        </Tag>
                    </Form.Item>
                )}
                <Form.Item name="content">
                    <TextEditor
                        placeholder="Nội dung"
                        onChange={() => {}}
                        value=""
                    />
                </Form.Item>
                <Form.Item style={{ marginBottom: 0 }}>
                    <Button htmlType="submit" type="primary">
                        Thêm bình luận
                    </Button>
                </Form.Item>
            </Form>
        );
        return (
            <div className={classes.comment}>
                <List
                    className={classes.list_comment}
                    header={`${comments.length} lượt bình luận`}
                    itemLayout="horizontal"
                    dataSource={comments}
                    renderItem={item => {
                        const { parent, id } = item;
                        const childComment = comments
                            .filter(comment => {
                                return comment.parent === id;
                            })
                            .map(comment => {
                                return (
                                    <Comment
                                        content={comment.content}
                                        actions={[
                                            <span
                                                key="comment-list-reply-to-0"
                                                onClick={() =>
                                                    handleReply(
                                                        comment.author,
                                                        comment.parent
                                                    )
                                                }
                                            >
                                                Phản hồi
                                            </span>,
                                        ]}
                                        author={comment.author}
                                        avatar={comment.avatar}
                                        datetime={comment.datetime}
                                    />
                                );
                            });
                        if (!parent) {
                            return (
                                <li>
                                    <Comment
                                        actions={item.actions}
                                        author={item.author}
                                        avatar={item.avatar}
                                        content={item.content}
                                        datetime={item.datetime}
                                    >
                                        {childComment}
                                    </Comment>
                                </li>
                            );
                        }
                    }}
                />
                <p className={classes.add_comment_text}>Thêm bình luận</p>
                <Comment content={<Editor onSubmit={handleSubmitComment} />} />
            </div>
        );
    };

    const [likeSongMutation, { loading, error }] = useMutation(
        LIKE_SONG_MUTATION
    );

    const [
        unlikeSongMutation,
        { loading: unlikeLoading, error: unlikeError },
    ] = useMutation(UNLIKE_SONG_MUTATION);

    useEffect(() => {
        refetch();
    }, [data.like]);

    const likeSection: Function = (): JSX.Element => {
        if (isLoading) return null;
        if (profileId) {
            let actionMutation: Function;
            const listLikedUser: Array<string> = data.listLikedUser;
            let disabled: disableStatus = disableStatus.AVAILABLE;
            let color: string;
            if (listLikedUser.indexOf(profileId) > -1) {
                disabled = disableStatus.LIKED;
                actionMutation = unlikeSongMutation;
            } else {
                disabled = disableStatus.AVAILABLE;
                actionMutation = likeSongMutation;
            }
            if (data._id === profileId) {
                disabled = disableStatus.OWNER;
            }
            switch (disabled) {
                case disableStatus.AVAILABLE:
                    color = '#111111';
                    break;
                case disableStatus.LIKED:
                    color = '#ff5286';
                    break;
                case disableStatus.OWNER:
                    color = '#d3d3d3';
                    break;
                default:
                    color = '#111111';
                    break;
            }
            return (
                <Item
                    key="like"
                    disabled={disabled === disableStatus.OWNER}
                    onClick={async () => {
                        try {
                            await actionMutation({
                                variables: { song_id: data._id },
                            });
                            if (refetch) {
                                refetch();
                            }
                        } catch (error) {
                            notification.error({
                                message: error.message,
                            });
                        }
                    }}
                >
                    <Icon component={HeartSvg} style={{ color }} />
                    {disabled === disableStatus.LIKED ? 'Bỏ thích' : 'Thích'}
                </Item>
            );
        }
    };

    const menu = (
        <Menu>
            <Item key="share">
                <Icon component={ShareSvg} />
                Chia sẻ
            </Item>
            <Divider />
            {likeSection()}
            <Divider />
            <Item key="add_to_playlist" onClick={showAddModal}>
                <Icon component={PlaylistSvg} />
                Thêm vào playlist
            </Item>
            <Divider />
            <Item
                key="download"
                onClick={() => {
                    fetch(
                        'https://cors-anywhere.herokuapp.com/' + data.song_url,
                        {
                            method: 'GET',
                            headers: {
                                'Content-Type': 'audio/mpeg',
                            },
                        }
                    )
                        .then(response => response.blob())
                        .then(blob => {
                            if (process.browser) {
                                const url = window.URL.createObjectURL(
                                    new Blob([blob])
                                );
                                const link = document.createElement('a');
                                link.href = url;
                                link.setAttribute(
                                    'download',
                                    `${data.song_name}.mp3`
                                );
                                document.body.appendChild(link);
                                link.click();
                                link.parentNode.removeChild(link);
                            }
                        });
                }}
            >
                <Icon component={DownloadSvg} />
                Tải xuống
            </Item>
            <Divider />
            <Item
                key="comments"
                onClick={showModal}
                disabled={loadingComment || !!errorComment}
            >
                <Icon component={CommentSvg} />
                Bình luận
            </Item>
            <Item key="report">
                <Icon component={FlagSvg} />
                Báo cáo vi phạm
            </Item>
        </Menu>
    );

    return (
        <div className={classes.root}>
            <div className={classes.thumbnail}>
                <div className={classes.image_container}>
                    <img
                        className={classes.image}
                        src={
                            data.song_image_url ||
                            '/static/images/song-placeholder.png'
                        }
                    />
                </div>
                <div className={classes.thumbnail__overlay}>
                    <Icon
                        onClick={() => {
                            localStorage.setItem('song', JSON.stringify(data));
                            dispatch({
                                type: 'SET_CURRENT_TIME',
                                payload: 0,
                            });
                            dispatch({
                                type: 'SET_CURRENT_SONG',
                                payload: {
                                    song_url: data.song_url,
                                    song_image_url: data.song_image_url,
                                    song_name: data.song_name,
                                    artist: data.author,
                                    like: data.like,
                                    listen: data.listen,
                                    uploader: data.uploader,
                                    song_id: data._id,
                                },
                            });
                            listenSongMutation({
                                variables: {
                                    song_id: data._id,
                                },
                            });
                        }}
                        component={PlaySvg}
                        style={{ fontSize: `64px`, color: '#fe0068' }}
                    />
                    <div className={classes.thumbnail__duration}>
                        {data.duration ? parseTime(data.duration) : `0:00`}
                    </div>
                </div>
            </div>
            <div className={classes.info}>
                <span className={classes.time}>
                    <Icon
                        component={CalendarSvg}
                        style={{
                            color: '#fe0068',
                            marginRight: '1rem',
                        }}
                    />
                    {moment(data.createdAt).format('DD-MM-YYYY')}
                </span>
                <div className={classes.song_name}>
                    {data.song_name}{' '}
                    <Dropdown overlay={menu} trigger={['click']}>
                        <a
                            className={classes.action}
                            onClick={e => e.preventDefault()}
                        >
                            <Icon component={ActionSvg} />
                        </a>
                    </Dropdown>
                </div>
                <div className={classes.information}>
                    <div className={classes.information__author}>
                        <span>{data.author}</span>
                    </div>
                    <div className={classes.information__reaction}>
                        <span className={classes.information__statistical}>
                            <Icon
                                component={ListenSvg}
                                style={{
                                    marginLeft: '0.25rem!important',
                                }}
                            />{' '}
                            {data.listen}
                        </span>
                        <span className={classes.information__statistical}>
                            <Icon
                                component={HeartSvg}
                                style={{
                                    marginLeft: '0.25rem!important',
                                }}
                            />{' '}
                            {data.like}
                        </span>
                        <span className={classes.information__statistical}>
                            <Icon
                                component={CommentSvg}
                                style={{
                                    marginLeft: '0.25rem!important',
                                }}
                            />{' '}
                            {data.listComment?.length || 0}
                        </span>
                    </div>
                </div>
            </div>
            <Modal
                title="Bình luận"
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
            >
                {commentModal()}
            </Modal>
            <Modal
                title="Thêm bài hát vào playlist"
                visible={isAddModalVisible}
                onOk={handleAddOK}
                onCancel={handleAddCancel}
            >
                <AddToPlaylistModal song_id={data._id} />
            </Modal>
        </div>
    );
};

export default Song;
