import React, { FC } from 'react';
import classes from './styles.module.scss';
import { Tabs, List } from 'antd';
import { useQuery } from '@apollo/react-hooks';
import GET_LIST_FOLLOWERS from '@/queries/queries/GetListFollowers.graphql';
import GET_LIST_FOLLOWINGS from '@/queries/queries/GetListFollowings.graphql';
import FollowItem from './FollowItem';

const { TabPane } = Tabs;

const Follow: FC = (): JSX.Element => {
    const {
        data: followersData,
        loading: followersLoading,
        error: followersError,
        refetch: fetchFollower,
    } = useQuery(GET_LIST_FOLLOWERS, {
        fetchPolicy: 'network-only',
    });

    const {
        data: followingsData,
        loading: followingsLoading,
        error: followingsError,
        refetch: fetchFollowing,
    } = useQuery(GET_LIST_FOLLOWINGS, {
        fetchPolicy: 'network-only',
    });

    if (
        followersLoading ||
        followersError ||
        followingsLoading ||
        followingsError
    ) {
        return <p>aaa</p>;
    }

    if (!followersData || !followingsData) {
        return <p>Ko co data</p>;
    }

    const FollowersSession: FC = (): JSX.Element => {
        return (
            <div className={classes.followers_session}>
                {followersData.getListFollowers.map(user => {
                    return (
                        <FollowItem
                            data={user}
                            fetch={fetchFollower}
                            fetchFollowing={fetchFollowing}
                        />
                    );
                })}
            </div>
        );
    };

    const FollowingsSession: FC = (): JSX.Element => {
        return (
            <div className={classes.followers_session}>
                {followingsData.getListFollowings.map(user => {
                    return <FollowItem data={user} fetch={fetchFollowing} />;
                })}
            </div>
        );
    };

    return (
        <div className={classes.root}>
            <h1 className={classes.tab}>
                <Tabs defaultActiveKey="1" size="large">
                    <TabPane tab="Danh sách người theo dõi bạn" key="1">
                        <h1 className={classes.title}>
                            Danh sách người theo dõi bạn
                        </h1>
                        <FollowersSession />
                    </TabPane>
                    <TabPane tab="Danh sách người bạn đang theo dõi" key="2">
                        <h1 className={classes.title}>
                            Danh sách người bạn đang theo dõi
                        </h1>
                        <FollowingsSession />
                    </TabPane>
                </Tabs>
            </h1>
        </div>
    );
};

export default Follow;
