export interface IFollowItem {
    id: string;
    account_name: string;
    first_name: string;
    last_name: string;
    email: string;
    age: number;
    gender: boolean;
    birthday: Date;
    avatarUrl: string;
    account_name: string;
}
