import React, { FC, Fragment, useCallback, useEffect } from 'react';
import Link from 'next/link';
import * as PropTypes from 'prop-types';
import classes from './styles.module.scss';
import { IFollowItem } from './type';
import { List, message, Avatar, Button } from 'antd';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import CHECK_FOLLOW from '@/queries/CheckFollow.graphql';
import FOLLOW_MUTATION from '@/queries/Follow.graphql';
import UN_FOLLOW_MUTATION from '@/queries/UnFollow.graphql';
import LoadingIndicator from '../../base/loadingIndicator';
import * as routes from '../../../constants/routes/routes';

const { Item } = List;
const { Meta } = Item;
const AVATAR_SIZE: number = 90;

interface IFollowItemProps {
    data: IFollowItem;
    fetch: Function;
    fetchFollowing?: Function;
}

const FollowItem: FC<IFollowItemProps> = (props: IFollowItemProps) => {
    const { data, fetch, fetchFollowing } = props;
    const {
        id,
        account_name,
        first_name,
        last_name,
        email,
        age,
        gender,
        birthday,
        avatarUrl,
    } = data;

    const [
        runFollowQuery,
        { data: isFollow, loading: isFollowLoading, error: isFollowError },
    ] = useLazyQuery(CHECK_FOLLOW, {
        fetchPolicy: 'network-only',
    });

    const [
        followMutation,
        { loading: followLoading, error: followError },
    ] = useMutation(FOLLOW_MUTATION);

    const [
        unFollowMutation,
        { loading: unFollowLoading, error: unFollowError },
    ] = useMutation(UN_FOLLOW_MUTATION);

    const handleFollow = useCallback(async () => {
        if (id) {
            await followMutation({
                variables: {
                    follow_id: id,
                },
            });
            await runFollowQuery({
                variables: {
                    follow_id: id,
                },
            });
            if (fetch) {
                fetch();
            }
            if (fetchFollowing) {
                fetchFollowing();
            }
        }
    }, [followMutation, id, isFollow, fetch]);

    const handleUnFollow = useCallback(async () => {
        if (id) {
            await unFollowMutation({
                variables: {
                    follow_id: id,
                },
            });
            await runFollowQuery({
                variables: {
                    follow_id: id,
                },
            });
            if (fetch) {
                fetch();
            }
            if (fetchFollowing) {
                fetchFollowing();
            }
        }
    }, [unFollowMutation, id, isFollow, fetch]);

    useEffect(() => {
        if (id)
            runFollowQuery({
                variables: {
                    follow_id: id,
                },
            });
    }, [id]);

    const AvatarComponent: FC = (): JSX.Element => {
        if (data) {
            const { avatarUrl, account_name } = data;
            if (!avatarUrl)
                return (
                    <Avatar size={AVATAR_SIZE}>
                        <p className={classes.avatar_text}>
                            {account_name[0].toUpperCase()}
                        </p>
                    </Avatar>
                );
            return <Avatar src={data && data.avatarUrl} size={AVATAR_SIZE} />;
        }
    };

    if (isFollowLoading)
        return <LoadingIndicator loadingTitle="Loading profile" />;
    if (isFollowError)
        return <div className={classes.error_page}>Error when fetch data</div>;
    if (!isFollow) {
        return <div className={classes.error_page}>Error when fetch data</div>;
    }

    const checkFollow = isFollow ? isFollow.checkFollow : undefined;

    return (
        <div className={classes.root}>
            <Item key={id}>
                <Meta
                    avatar={<AvatarComponent />}
                    title={
                        <Link href={`${routes.USER_PROFILE}/${id}`}>
                            <span className={classes.account_name_text}>
                                {account_name}
                            </span>
                        </Link>
                    }
                    description={
                        <Fragment>
                            <p className={classes.name_text}>
                                {`${first_name} ${last_name}`}
                            </p>
                        </Fragment>
                    }
                />
                <Button
                    className={classes.follow_button}
                    onClick={async () => {
                        checkFollow === 'true'
                            ? handleUnFollow()
                            : handleFollow();
                    }}
                    loading={followLoading || unFollowLoading}
                >
                    {checkFollow === 'true' ? 'Bỏ theo dõi' : 'Theo dõi'}
                </Button>
            </Item>
        </div>
    );
};

FollowItem.propTypes = {};

export default FollowItem;
