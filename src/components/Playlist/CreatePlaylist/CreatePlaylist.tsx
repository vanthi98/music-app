import React, { FC, useCallback, useState } from 'react';
import classes from './styles.module.scss';
import { Button, Modal, Form, Input, notification } from 'antd';
import Icon from '@ant-design/icons';
import PlusSvg from 'public/static/icons/plus.svg';
import { useMutation } from '@apollo/react-hooks';
import CREATE_PLAYLIST_MUTATION from '@/queries/CreatePlaylist.graphql';

const CreatePlaylist: FC<{ refetch: Function }> = ({
    refetch,
}): JSX.Element => {
    const [isModalVisible, setIsModalVisible] = useState<boolean | undefined>(
        false
    );
    const [form] = Form.useForm();
    const [createPlaylistMutation, { loading, error }] = useMutation(
        CREATE_PLAYLIST_MUTATION
    );

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleCreatePlaylist = useCallback(
        async formValues => {
            const { playlist_name } = formValues;
            await createPlaylistMutation({
                variables: {
                    name: playlist_name,
                },
            });
            if (refetch) {
                refetch();
            }
            setIsModalVisible(false);
            form.resetFields();
            notification.success({
                message: `Tạo playlist '${playlist_name}' thành công`,
            });
        },
        [createPlaylistMutation, refetch, setIsModalVisible]
    );

    return (
        <div className={classes.root}>
            <Button className={classes.button} onClick={showModal}>
                <Icon
                    component={PlusSvg}
                    style={{ fontSize: '1.5rem' }}
                    className={classes.button__icon}
                />
                <span className={classes.button__text}>Tạo playlist mới</span>
            </Button>
            <Modal
                title="Tạo playlist mới"
                visible={isModalVisible}
                onCancel={handleCancel}
                footer={[
                    <Button
                        type="primary"
                        htmlType="submit"
                        onClick={handleCancel}
                    >
                        Hủy
                    </Button>,
                ]}
            >
                <Form form={form} onFinish={handleCreatePlaylist}>
                    <Form.Item
                        name="playlist_name"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập tên playlist',
                            },
                        ]}
                    >
                        <Input placeholder="Nhập tên playlist" />
                    </Form.Item>
                    <Form.Item style={{ marginTop: '1rem' }}>
                        <Button type="primary" htmlType="submit" size={'large'}>
                            Tạo playlist
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
};

export default CreatePlaylist;
