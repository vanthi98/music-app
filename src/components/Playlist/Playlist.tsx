import React, { FC, useEffect } from 'react';
import classes from './styles.module.scss';
import { Row, Col } from 'antd';
import LoadingIndicator from '@/components/base/loadingIndicator';
import { IPlaylistProps } from './Playlist.type';
import { useQuery } from '@apollo/react-hooks';
import GET_ALL_PLAYLIST from '@/queries/GetAllPlaylist.graphql';
import CreatePlaylist from './CreatePlaylist';
import PlaylistItem from './PlaylistItem';

const Playlist: FC<IPlaylistProps> = ({}): JSX.Element => {
    const { data: playlists, loading, error, refetch } = useQuery(
        GET_ALL_PLAYLIST,
        {
            fetchPolicy: 'network-only',
        }
    );

    useEffect(() => {
        refetch();
    }, []);

    if (loading) return <LoadingIndicator loadingTitle="loading history" />;
    if (error)
        return <div className={classes.error_page}>Error when fetch data</div>;
    if (!playlists) {
        return <div className={classes.error_page}>Error when fetch data</div>;
    }

    let list: JSX.Element;

    if (playlists) {
        list = (
            <div className={classes.content__list} id="list">
                <h1 className={classes.title}>Danh sách playlist của bạn</h1>
                <Row
                    gutter={[
                        { xs: 16, sm: 24, md: 32, lg: 40 },
                        { xs: 16, sm: 24, md: 32, lg: 40 },
                    ]}
                >
                    <Col xs={12} sm={12} md={8} lg={6} xl={4}>
                        <CreatePlaylist refetch={refetch} />
                    </Col>
                </Row>
                <Row
                    gutter={[
                        { xs: 16, sm: 24, md: 32, lg: 40 },
                        { xs: 16, sm: 24, md: 32, lg: 40 },
                    ]}
                    style={{ marginTop: '2rem' }}
                >
                    {playlists.getAllPlaylist.length > 0 ? (
                        playlists.getAllPlaylist.map(playlist => {
                            return (
                                <Col xs={12} sm={12} md={8} lg={6} xl={4}>
                                    <PlaylistItem
                                        playlist={playlist}
                                        refetch={refetch}
                                    />
                                </Col>
                            );
                        })
                    ) : (
                        <h1 className={classes.no_playlist_text}>
                            Bạn chưa có playlist nào
                        </h1>
                    )}
                </Row>
            </div>
        );
    }

    return <div className={classes.root}>{list}</div>;
};

export default Playlist;
