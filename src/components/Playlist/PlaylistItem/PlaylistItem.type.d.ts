interface IPlaylist {
    name: string;
    listSong: Array<string>;
    _id: string;
}

export interface IPlaylistItemProps {
    playlist: IPlaylist;
    refetch: Function;
}
