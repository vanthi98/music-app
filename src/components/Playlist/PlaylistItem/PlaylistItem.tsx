import React, { FC, useEffect } from 'react';
import classes from './styles.module.scss';
import { IPlaylistItemProps } from './PlaylistItem.type';
import GET_SONG from '@/queries/GetSong.graphql';
import DELETE_PLAYLIST from '@/queries/DeletePlaylist.graphql';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { Dropdown, Menu, Modal, notification } from 'antd';
import Icon from '@ant-design/icons';
import InfoSvg from '@public/static/icons/info.svg';
import ActionSvg from '@public/static/icons/action.svg';
import TrashSvg from '@public/static/icons/trash.svg';
import { useRouter } from 'next/router';

const { Item, Divider } = Menu;

const PlaylistItem: FC<IPlaylistItemProps> = ({
    playlist,
    refetch,
}): JSX.Element => {
    const router = useRouter();
    const { name, listSong, _id } = playlist;
    const { data: song, loading, error } = useQuery(GET_SONG, {
        fetchPolicy: 'network-only',
        variables: {
            song_id: listSong[0],
        },
    });

    const [
        deletePlayListMutation,
        { loading: deletePlaylistLoading, error: deletePlaylistError },
    ] = useMutation(DELETE_PLAYLIST);

    const handleDeletePlaylist = () => {
        Modal.confirm({
            content: 'Bạn có chắc muốn xóa playlist này ?',
            async onOk() {
                await deletePlayListMutation({
                    variables: {
                        playlist_id: _id,
                    },
                });
                if (refetch) {
                    refetch();
                }
                notification.success({
                    message: `Xóa playlist '${name}' thành công`,
                });
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    };

    const handleViewDetail = () => {
        console.log('ccc');
        router.push(`/playlist-detail/${_id}`);
    };

    const thumbnail = (): JSX.Element => {
        if (error || loading || !song)
            return (
                <img
                    src="https://via.placeholder.com/500"
                    className={classes.image}
                />
            );

        if (song) {
            return (
                <img
                    src={song.getSong.song_image_url}
                    className={classes.image}
                />
            );
        }
    };

    const menu: JSX.Element = (
        <Menu>
            <Item key="detail" onClick={handleViewDetail}>
                <Icon component={InfoSvg} />
                Chi tiết
            </Item>
            <Divider />
            <Item key="delete" onClick={handleDeletePlaylist}>
                <Icon component={TrashSvg} />
                Xóa playlist
            </Item>
            <Divider />
        </Menu>
    );

    return (
        <div className={classes.root}>
            <div className={classes.playlist}>
                <div className={classes.playlist__thumbnail}>{thumbnail()}</div>
                <div className={classes.playlist__info}>
                    <span className={classes.playlist__name}>{name}</span>
                    <Dropdown overlay={menu} trigger={['click']}>
                        <a
                            className={classes.action}
                            onClick={e => e.preventDefault()}
                        >
                            <Icon component={ActionSvg} />
                        </a>
                    </Dropdown>
                </div>
            </div>
        </div>
    );
};

export default PlaylistItem;
