import React, { FC, useState } from 'react';
import Router, { useRouter } from 'next/router';
import classes from './styles/login.module.scss';
import { Form, Input, Col, Row, Checkbox } from 'antd';
import Button from '../base/button';
import PropTypes from 'prop-types';
import { useLogin } from '../../containers/users/useLogin';
import Icon, {
    UserOutlined,
    EyeInvisibleOutlined,
    EyeTwoTone,
    LockOutlined,
} from '@ant-design/icons';
import { checkSignedIn } from '../../utils/checkSignIn';
import Link from 'next/link';
import LoadingIndicator from '../base/loadingIndicator';
import BackgroundImage from '../../../public/static/images/bg.png';
import UsersSvg from './assets/icons/users.svg';
import PasswordSvg from './assets/icons/unlock-alt.svg';
import VisibleSvg from './assets/icons/eye.svg';
import InvisibleSvg from './assets/icons/eye-slash.svg';
import * as routes from '../../constants/routes/routes';
import AccountWrapper from '../common/accountWrapper';
interface LoginProps {
    title: string;
}
interface User {
    name: string;
    age: number;
    gender: boolean;
}
interface Account {
    account_name: string;
    password: string;
}

const layout: Array<number> = [24, 24, 6, 6, 5, 4];

const Login: FC<LoginProps> = ({ title }: LoginProps) => {
    const router = useRouter();
    const afterSubmit = () => {
        setTimeout(() => {
            router.push(routes.HOME);
        }, 500);
        Router.reload();
    };
    const hookProps = useLogin({ afterSubmit });
    const { errors, loginLoading, handleLogin } = hookProps;

    if (loginLoading) {
        return <LoadingIndicator loadingTitle={'Login...'} />;
    }
    const errorMessageList =
        errors.length > 0 ? (
            <ul className={classes.error_list}>
                {errors.map(error => {
                    return <li>{error}</li>;
                })}
            </ul>
        ) : null;

    return (
        <>
            <AccountWrapper layout={layout}>
                <div className={classes.root__form_container}>
                    <Link href={routes.HOME}>
                        <img
                            src={'/static/images/logo-alt.png'}
                            className={classes.root__form_container__logo}
                        />
                    </Link>
                    <Form
                        className={classes.root__form_container__login_form}
                        onFinish={handleLogin}
                        initialValues={{
                            email: '',
                            password: '',
                        }}
                        layout={'vertical'}
                    >
                        <Form.Item
                            label={
                                <span
                                    className={
                                        classes.root__form_container__login_form__label
                                    }
                                >
                                    {'Email'}
                                </span>
                            }
                            name="email"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập email của bạn',
                                },
                            ]}
                        >
                            <div
                                className={
                                    classes.root__form_container__login_form__label__input_container
                                }
                            >
                                <Input
                                    id={'login_user_input'}
                                    size="large"
                                    placeholder="Email"
                                    prefix={
                                        <Icon
                                            component={UsersSvg}
                                            style={{
                                                marginRight:
                                                    '0.375rem!important',
                                                color: '#fe0068',
                                            }}
                                        />
                                    }
                                    bordered={false}
                                    className={classes.account_name_input}
                                    autoComplete="off"
                                />
                            </div>
                        </Form.Item>
                        <Form.Item
                            id={'login_password_control'}
                            label={
                                <span
                                    className={
                                        classes.root__form_container__login_form__label
                                    }
                                >
                                    {'Mật khẩu'}
                                </span>
                            }
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập mật khẩu',
                                },
                            ]}
                        >
                            <div
                                className={
                                    classes.root__form_container__login_form__label__input_container
                                }
                            >
                                <Input.Password
                                    id={'login_password_input'}
                                    size="large"
                                    placeholder="Mật khẩu"
                                    iconRender={visible =>
                                        visible ? (
                                            <Icon
                                                component={VisibleSvg}
                                                style={{
                                                    color: '#333333',
                                                }}
                                            />
                                        ) : (
                                            <Icon
                                                component={InvisibleSvg}
                                                style={{
                                                    color: '#333333',
                                                }}
                                            />
                                        )
                                    }
                                    prefix={
                                        <Icon
                                            component={PasswordSvg}
                                            style={{
                                                marginRight:
                                                    '0.375rem!important',
                                                color: '#fe0068',
                                            }}
                                        />
                                    }
                                    bordered={false}
                                    className={classes.password_input}
                                />
                            </div>
                        </Form.Item>
                        <Row
                            className={classes.action}
                            justify="space-between"
                            align="top"
                        >
                            <Col>
                                <Form.Item
                                    className={classes.action__remember}
                                    name="remember"
                                    valuePropName="checked"
                                >
                                    <Checkbox>
                                        <span
                                            className={
                                                classes.action__remember_text
                                            }
                                        >
                                            Ghi nhớ mật khẩu
                                        </span>
                                    </Checkbox>
                                </Form.Item>
                            </Col>
                            <Col>
                                <Form.Item
                                    className={classes.action__forgot_password}
                                >
                                    <Link href={routes.FORGOT_PASSWORD}>
                                        <Button
                                            type="link"
                                            block
                                            className={
                                                classes.forgot_password_button
                                            }
                                        >
                                            Quên mật khẩu
                                        </Button>
                                    </Link>
                                </Form.Item>
                            </Col>
                        </Row>
                        <div className={classes.action_buttons}>
                            <Form.Item
                                className={classes.action_buttons__login_button}
                            >
                                <Button
                                    // type="primary"
                                    htmlType={'submit'}
                                    size={'large'}
                                    className={classes.login_button}
                                >
                                    Đăng nhập
                                </Button>
                            </Form.Item>
                            <Form.Item
                                className={
                                    classes.action_buttons__signup_button
                                }
                            >
                                <Link href="signup">
                                    <Button
                                        type="link"
                                        className={
                                            classes.create_account_button
                                        }
                                    >
                                        Tạo tài khoản
                                    </Button>
                                </Link>
                            </Form.Item>
                        </div>
                    </Form>

                    {errorMessageList}
                </div>
            </AccountWrapper>
        </>
    );
};

Login.defaultProps = {
    title: '',
};

Login.propTypes = {
    title: PropTypes.string.isRequired,
};

export default Login;
