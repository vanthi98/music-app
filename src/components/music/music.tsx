import React, { FC, useState, useRef, useEffect } from 'react';
import Link from 'next/link';
import { useSelector, useDispatch } from 'react-redux';
import classes from './styles/music.module.scss';
import {
    Button,
    Slider,
    Select,
    Popover,
    Row,
    Col,
    Menu,
    Dropdown,
} from 'antd';
import Icon, {
    CaretRightFilled,
    PauseOutlined,
    StepBackwardOutlined,
    StepForwardOutlined,
} from '@ant-design/icons';
import { Song } from './interfaces/songs.interface';
import parseTime from '../../utils/pasreTime';
import RandomSvg from './random.svg';
import VolumeSvg from './volume.svg';
import UndoSvg from './undo.svg';
import HeartSvg from './heart.svg';
import CommentSvg from './comment.svg';
import ActionSvg from './action.svg';
import PlaylistSvg from './playlist.svg';
import DownloadSvg from './download.svg';
import ListenSvg from './listen.svg';
import UserSvg from './user.svg';
import * as routes from '../../constants/routes/routes';
import { useMutation, useQuery } from '@apollo/react-hooks';
import LIKE_SONG_MUTATION from '../../queries/LikeSong.graphql';
import GET_PROFILE_BY_EMAIL from '../../queries/GetProfileByEmail.graphql';

const { Option } = Select;
const { Item } = Menu;

interface MusicProps {}

const ISSERVER = typeof window === 'undefined';

const mockData: Song = {
    title: 'Nơi này có anh',
    urlSong: 'noinaycoanh',
    urlImage: 'noinaycoanh',
    altImage: 'Nơi này có anh',
    artist: 'Sơn Tùng MTP',
};

const Music: FC<MusicProps> = () => {
    // let song = '';
    // if (!ISSERVER) song = localStorage.getItem('song');
    const currentSongHook = useSelector(state => state.song.song);
    const currentSong = currentSongHook;
    const currentTimeState = useSelector(state => state.song.currentTime);
    const audioRef = useRef(null);
    const dispatch = useDispatch();
    const [duration, setDuration] = useState<number | null>(0);
    const [currentTime, setCurrentTime] = useState<number | null>(
        currentTimeState
    );
    const [isPlay, setPlay] = useState<boolean | null>(false);
    const [volume, setVolume] = useState<number | null>(50);

    const {
        data: profileData,
        loading: profileLoading,
        error: profileError,
    } = useQuery(GET_PROFILE_BY_EMAIL, {
        fetchPolicy: 'network-only',
        variables: {
            email: currentSong.uploader,
        },
    });

    const handleLoadedData: VoidFunction = () => {
        setDuration(audioRef.current.duration);
        if (isPlay) audioRef.current.play();
    };

    const handlePausePlayClick: VoidFunction = () => {
        if (isPlay) {
            audioRef.current.pause();
        } else {
            audioRef.current.play();
        }
        if (!Number.isNaN(volume)) {
            setVolume(volume);
            audioRef.current.volume = volume / 100;
        }
        setPlay(!isPlay);
    };

    const onGoToTime = value => {
        audioRef.current.currentTime = value;
        setCurrentTime(value);
    };

    const action: JSX.Element = (
        <div>
            <p>
                <Icon component={UserSvg} />{' '}
                <Link
                    href={`${routes.USER_PROFILE}/${
                        profileData?.getProfileByEmail.id || ''
                    }`}
                >
                    {profileData?.getProfileByEmail.account_name || ''}
                </Link>
            </p>
            <span style={{ marginRight: '1rem' }}>
                <Icon component={HeartSvg} /> {currentSong.like}
            </span>
            <span>
                <Icon component={ListenSvg} /> {currentSong.listen}
            </span>
            <Menu>
                <Item key="add_to_playlist">
                    <Icon component={PlaylistSvg} />
                    Add to playlist
                </Item>
                <Item
                    key="download"
                    onClick={() => {
                        fetch(
                            'https://cors-anywhere.herokuapp.com/' +
                                currentSong.song_url,
                            {
                                method: 'GET',
                                headers: {
                                    'Content-Type': 'audio/mpeg',
                                },
                            }
                        )
                            .then(response => response.blob())
                            .then(blob => {
                                if (process.browser) {
                                    const url = window.URL.createObjectURL(
                                        new Blob([blob])
                                    );
                                    const link = document.createElement('a');
                                    link.href = url;
                                    link.setAttribute(
                                        'download',
                                        `${currentSong.song_name}.mp3`
                                    );
                                    document.body.appendChild(link);
                                    link.click();
                                    link.parentNode.removeChild(link);
                                }
                            });
                    }}
                >
                    <Icon component={DownloadSvg} />
                    Download
                </Item>
            </Menu>
        </div>
    );

    useEffect(() => {
        setDuration(Math.ceil(audioRef.current.duration));
    }, [audioRef]);

    useEffect(() => {
        setPlay(true);
    }, []);

    const handleUpdateTime = () => {
        setCurrentTime(Math.ceil(audioRef.current.currentTime));
        dispatch({
            type: 'SET_CURRENT_TIME',
            payload: Math.ceil(audioRef.current.currentTime),
        });
    };

    const onChangeVolume = value => {
        audioRef.current.volume = value / 100;
        setVolume(value);
    };

    const handleChangeSpeed = value => {
        audioRef.current.playbackRate = value;
    };

    const handleRePlay = () => {
        audioRef.current.currentTime = 0;
        setCurrentTime(0);
    };

    const volumeComponent = (
        <Slider
            min={0}
            max={100}
            value={volume}
            onChange={onChangeVolume}
            className={classes.volume_bar}
            vertical
        />
    );

    return (
        <Row className={classes.root} gutter={{ xs: 8, sm: 16, md: 24 }}>
            <Col className={classes.audio_html_container}>
                <audio
                    src={currentSong.song_url}
                    ref={audioRef}
                    controls
                    autoPlay
                    onLoadedData={handleLoadedData}
                    onTimeUpdate={handleUpdateTime}
                />
            </Col>
            <Col className={`${classes.info_container} gutter-row`} span={4}>
                <img
                    src={
                        currentSong.song_image_url ||
                        'https://via.placeholder.com/150'
                    }
                    alt={`${mockData.altImage}`}
                    width={50}
                    height={50}
                    className={classes.song_picture}
                />
                <div className={classes.title_and_artist_container}>
                    <p className={classes.title}>{currentSong.song_name}</p>
                    <p className={classes.artist}>{currentSong.author}</p>
                </div>
            </Col>
            <Col className={`${classes.actions_container} gutter-row`} span={2}>
                <Icon
                    component={HeartSvg}
                    onClick={() => {}}
                    className={classes.heart_button}
                />
                <Popover content={action} placement="top" trigger="click">
                    <Icon
                        component={ActionSvg}
                        onClick={() => {}}
                        className={classes.action_button}
                    />
                </Popover>
            </Col>
            <Col
                className={`${classes.controllers_container} gutter-row`}
                span={4}
            >
                <Icon
                    component={UndoSvg}
                    onClick={handleRePlay}
                    className={classes.replay_button}
                />
                <StepBackwardOutlined
                    onClick={() => {}}
                    className={classes.backward_button}
                />
                <Button
                    onClick={handlePausePlayClick}
                    type={'primary'}
                    shape="circle"
                    icon={isPlay ? <PauseOutlined /> : <CaretRightFilled />}
                    className={classes.pause_button}
                />
                <StepForwardOutlined
                    onClick={() => {}}
                    className={classes.forward_button}
                />
                <Icon component={RandomSvg} className={classes.random_button} />
            </Col>
            <Col
                className={`${classes.duration_container} gutter-row`}
                span={10}
            >
                <p className={classes.current_time}>{parseTime(currentTime)}</p>
                <Slider
                    min={0}
                    max={duration + 1}
                    value={currentTime}
                    onChange={onGoToTime}
                    className={classes.duration_bar}
                    tipFormatter={value => parseTime(value)}
                />
                <p className={classes.duration}>
                    {audioRef && audioRef.current
                        ? parseTime(Math.ceil(audioRef.current.duration))
                        : parseTime(duration)}
                </p>
            </Col>
            <Col className={`${classes.volume_container} gutter-row`} span={2}>
                <Popover
                    content={volumeComponent}
                    placement="top"
                    trigger="click"
                >
                    <Button
                        type="primary"
                        shape="round"
                        icon={
                            <Icon
                                component={VolumeSvg}
                                className={classes.volume_icon}
                            />
                        }
                        style={{ background: '#ff0068', border: 'none' }}
                    >
                        {volume}
                    </Button>
                </Popover>
            </Col>
            <Col className={`${classes.speed_container} gutter-row`} span={2}>
                <Select
                    defaultValue={1.0}
                    className={classes.speed_select}
                    onChange={handleChangeSpeed}
                >
                    <Option value={0.25}>0.25</Option>
                    <Option value={0.5}>0.5</Option>
                    <Option value={0.75}>0.75</Option>
                    <Option value={1.0}>1.0</Option>
                    <Option value={1.25}>1.25</Option>
                    <Option value={1.5}>1.5</Option>
                    <Option value={1.75}>1.75</Option>
                    <Option value={2.0}>2.0</Option>
                </Select>
            </Col>
        </Row>
    );
};

Music.defaultProps = {};

export default Music;
