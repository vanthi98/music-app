export interface Song {
    title: string;
    urlSong: string;
    urlImage: string;
    altImage: string;
    artist: string;
}
