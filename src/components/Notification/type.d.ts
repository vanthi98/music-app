export interface Data {
    notice_id: string;
    thumbnail?: string;
    title: string;
    description: string;
    createdAt: string;
    status: string;
}

export interface INotificationProps {
    data: Array<Data>;
    refetch: Function;
}
