import React, { FC, useEffect } from 'react';
import classes from './styles.module.scss';
import { List, Avatar, Spin, Popconfirm, message } from 'antd';
import Icon from '@ant-design/icons';
import moment from 'moment';
import { useSubscription, useMutation } from '@apollo/react-hooks';
import NOTIFICATION_ADDED from '@/queries/subscriptions/NotificationAdded.graphql';
import MARK_READ_NOTICE from '@/queries/mutations/MarkReadNotice.graphql';
import HIDE_NOTICE from '@/queries/mutations/HideNotice.graphql';
import { useProfile } from '@/containers/profiles/useProfile';
import { INotificationProps } from './type';
import TrashSvg from '@public/static/icons/trash.svg';
import BookmarkSvg from '@public/static/icons/bookmark.svg';

const Notification: FC<INotificationProps> = props => {
    const { data, refetch } = props;
    const hookProps = useProfile({});
    const { profileId, isLoading } = hookProps;

    const { data: notification } = useSubscription(NOTIFICATION_ADDED, {
        variables: {
            profile_id: profileId,
        },
    });

    const [markReadNotice, { loading: readNoticeLoading }] = useMutation(
        MARK_READ_NOTICE
    );
    const [hideNotice, { loading: hideNoticeLoading }] = useMutation(
        HIDE_NOTICE
    );

    useEffect(() => {
        if (refetch) {
            refetch();
        }
    }, [notification]);

    if (isLoading) {
        return (
            <div className={classes.root}>
                <Spin className={classes.spin} />
            </div>
        );
    }

    return (
        <div className={classes.root}>
            <div className={classes.header}>
                <p>
                    Thông báo (
                    {
                        data.filter(notice => notice.status === 'processing')
                            .length
                    }
                    /{data.filter(notice => notice.status !== 'hide').length})
                </p>
            </div>
            <List
                itemLayout="horizontal"
                dataSource={data}
                renderItem={item => {
                    const actions = [
                        <Icon
                            onClick={async () => {
                                await markReadNotice({
                                    variables: {
                                        notice_id: item.notice_id,
                                    },
                                });
                                if (refetch) {
                                    refetch();
                                }
                            }}
                            component={BookmarkSvg}
                        />,
                        <Popconfirm
                            title="Bạn có muốn xóa thông báo này ?"
                            onConfirm={async () => {
                                await hideNotice({
                                    variables: {
                                        notice_id: item.notice_id,
                                    },
                                });
                                if (refetch) {
                                    refetch();
                                }
                                message.success({
                                    content: `Xóa thông báo thành công`,
                                });
                            }}
                            okText="Xóa"
                            cancelText="Hủy"
                        >
                            <Icon component={TrashSvg} />
                        </Popconfirm>,
                    ];
                    if (item.status === 'read') actions.shift();
                    if (item.status === 'hide') return null;
                    return (
                        <List.Item
                            className={`${classes.notification_item} ${
                                item.status === 'read' ? classes.read : ''
                            }`}
                            actions={actions}
                        >
                            <List.Item.Meta
                                avatar={
                                    <Avatar
                                        src={
                                            item.thumbnail ||
                                            '/static/images/song-placeholder.png'
                                        }
                                        size={50}
                                    />
                                }
                                title={
                                    <span
                                        className={classes.notification_title}
                                    >
                                        {item.title}
                                    </span>
                                }
                                description={
                                    <div>
                                        <div className={classes.description}>
                                            {item.description}
                                        </div>
                                        <div className={classes.datetime}>
                                            {moment(item.createdAt).fromNow()}
                                        </div>
                                    </div>
                                }
                            />
                        </List.Item>
                    );
                }}
            />
            <div className={classes.footer}>
                <p>Đánh dấu đọc tất cả</p>
            </div>
        </div>
    );
};

export default Notification;
