import React, { FC } from 'react';
import classes from './styles/accountWrapper.module.scss';
import { Form, Input, Col, Row, Checkbox } from 'antd';
import Link from 'next/link';
import * as routes from '../../../constants/routes/routes';
import Icon from '@ant-design/icons';
import responsiveColumn from '../../../utils/responsiveColumn';

interface IAccountWrapperProps {
    layout: number[];
}

const AccountWrapper: FC<IAccountWrapperProps> = props => {
    const { children, layout } = props;
    const a = { ...responsiveColumn.apply(layout) };
    return (
        <>
            <div
                className={classes.root}
                style={{
                    backgroundImage: `url('/static/images/bg.png')`,
                }}
            >
                <Row justify="center" align="middle" style={{ height: '100%' }}>
                    <Col
                        {...responsiveColumn(
                            layout[0],
                            layout[1],
                            layout[2],
                            layout[3],
                            layout[4],
                            layout[5]
                        )}
                    >
                        {children}
                    </Col>
                </Row>
            </div>
        </>
    );
};

export default AccountWrapper;
