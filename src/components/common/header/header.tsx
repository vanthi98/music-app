import React, { FC, useState } from 'react';
import classes from './styles/header.module.scss';
import Image from 'next/image';
import { listMenu, defaultActiveMenu } from '../../../constants/menu/listMenu';
import { listAction } from '../../../constants/menu/listActions';
import { Row, Col, Input, Menu, Badge, Form, Dropdown, Spin } from 'antd';
import Icon, { SearchOutlined, UserOutlined } from '@ant-design/icons';
import NotificationSvg from '../../../../public/static/icons/bells.svg';
import responsiveColumn from '../../../utils/responsiveColumn';
import * as routes from '../../../constants/routes/routes';
import { useRouter } from 'next/router';
import Notification from '@/components/Notification';
import { useQuery } from '@apollo/react-hooks';
import GET_ALL_NOTIFICATION from '@/queries/queries/GetAllNotification.graphql';
import { checkSignedIn } from '@/utils/checkSignIn';

const actionItems = () => {
    return (
        <div className={classes.root__menu__actions__social}>
            {listAction.map((actionItem, index) => {
                return (
                    <Icon
                        key={actionItem.id}
                        component={actionItem.component}
                        style={{ fontSize: `16px` }}
                        className={classes.icon}
                    />
                );
            })}
        </div>
    );
};

const Header: FC = () => {
    const router = useRouter();
    const { asPath } = router;
    const isSignedIn = checkSignedIn();
    const [itemSelected, setItemSelected] = useState(defaultActiveMenu);
    const handleChangeItem = event => {
        setItemSelected(event.key);
    };

    const handleRedirectToHome = () => {
        router.push(routes.HOME);
    };

    const menuItems = () => {
        return listMenu.map(menuItem => {
            return (
                <Menu.Item
                    key={menuItem.key}
                    className={`${classes.root__menu__navbar__menu_item} ${
                        asPath === menuItem.url ? classes.active : ''
                    }`}
                    onClick={() => {
                        if (menuItem.url) router.push(menuItem.url);
                    }}
                >
                    {menuItem.tab}
                </Menu.Item>
            );
        });
    };

    const { data, loading, error, refetch } = useQuery(GET_ALL_NOTIFICATION, {
        fetchPolicy: 'network-only',
    });

    const notification: Function = (): JSX.Element => {
        if (loading) return <Spin />;
        if (error || !data) return <div>Không tìm thấy dữ liệu</div>;
        return (
            <Notification
                data={data?.getNoticeByCurrentAccount}
                refetch={refetch}
            />
        );
    };
    return (
        <Row className={classes.root}>
            <Col
                className={classes.root__logo_container}
                {...responsiveColumn(24, 24, 6, 6, 5, 4)}
                onClick={handleRedirectToHome}
            >
                <div className={classes.root__logo_container__logo}>
                    <Image
                        src={`/static/images/logo.png`}
                        alt={`logo`}
                        width={40}
                        height={40}
                    />
                </div>
                <h1>Mecha Music</h1>
            </Col>
            <Col
                className={classes.root__menu}
                {...responsiveColumn(0, 0, 18, 18, 19, 20)}
            >
                <div className={classes.root__menu__search_box}>
                    <Form>
                        <Input.Search
                            autoComplete="off"
                            onSearch={value => {
                                if (value)
                                    router.push({
                                        pathname: routes.SEARCH,
                                        query: { keyword: value },
                                    });
                            }}
                            allowClear
                            id="search_box"
                            placeholder="Tìm kiếm trong Mecha Music..."
                            bordered={false}
                            enterButton={
                                <SearchOutlined style={{ color: '#fe0068' }} />
                            }
                        />
                    </Form>
                </div>
                <div className={classes.root__menu__navbar}>
                    <Menu
                        mode="horizontal"
                        className={classes.root__menu__navbar__list}
                        //selectedKeys={[itemSelected]}
                        onClick={handleChangeItem}
                    >
                        {menuItems()}
                    </Menu>
                </div>
                <div className={classes.root__menu__actions}>
                    {actionItems()}
                </div>
                <div className={classes.root__menu__avatar}>
                    {isSignedIn ? (
                        <Dropdown
                            overlay={notification()}
                            trigger={['click']}
                            className={classes.dropdown}
                        >
                            <Badge
                                count={
                                    data?.getNoticeByCurrentAccount.filter(
                                        notice => notice.status !== 'hide'
                                    ).length
                                }
                                overflowCount={9}
                            >
                                <Icon
                                    component={NotificationSvg}
                                    style={{ fontSize: `32px` }}
                                    className={classes.icon}
                                />
                            </Badge>
                        </Dropdown>
                    ) : (
                        <Icon
                            component={NotificationSvg}
                            style={{ fontSize: `32px` }}
                            className={classes.icon}
                        />
                    )}
                </div>
            </Col>
        </Row>
    );
};

export default Header;
