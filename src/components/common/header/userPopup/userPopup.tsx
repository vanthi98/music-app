import React, { FC, useState } from 'react';
import { useRouter } from 'next/router';
import classes from './styles/userPopup.module.scss';
import Button from '../../../base/button';
import * as routes from '../../../../constants/routes/routes';

const UserPopup: FC = () => {
    const router = useRouter();
    const handleRedirectToLogin = () => {
        router.push(routes.LOGIN);
    };
    return (
        <div className={classes.root}>
            <Button
                type="primary"
                style={{ backgroundColor: '#fe0068', borderColor: '#fe0068' }}
                onClick={handleRedirectToLogin}
            >
                Đăng nhập
            </Button>
        </div>
    );
};

export default UserPopup;
