import React, { FC } from 'react';
import classes from './styles/menu.module.scss';
import { Menu, Spin, Avatar, Skeleton } from 'antd';
import Router, { useRouter } from 'next/router';
import { checkSignedIn } from '@/utils/checkSignIn';
import { useProfile } from '@/containers/profiles/useProfile';
import { UserOutlined } from '@ant-design/icons';
import * as routes from '@/constants/routes/routes';
import { useDispatch } from 'react-redux';

const SideBar: FC = () => {
    const isSignedIn = checkSignedIn();
    const dispatch = useDispatch();
    const router = useRouter();
    const { asPath } = router;
    const hookProps = useProfile({});
    const { profileData, isLoading } = hookProps;

    const handleClick = (e: any): void => {
        console.log('click ', e);
    };
    const handleLogout = () => {
        Router.reload();
        localStorage.removeItem('token');
        dispatch({
            type: 'SET_CURRENT_SONG',
            payload: {},
        });
        localStorage.removeItem('song');
        router.push(routes.LOGIN);
    };

    const handleRedirectToLogin = () => {
        localStorage.removeItem('song');
        dispatch({
            type: 'SET_CURRENT_SONG',
            payload: {},
        });
        router.push(routes.LOGIN);
    };

    const AvatarComponent: FC = () => {
        if (profileData) {
            const { avatarUrl, last_name } = profileData;
            if (!avatarUrl)
                return (
                    <Avatar size={160}>
                        <p className={classes.avatar_text}>
                            {last_name[0].toUpperCase()}
                        </p>
                    </Avatar>
                );
            return (
                <Avatar src={profileData && profileData.avatarUrl} size={160} />
            );
        }
        return <Avatar size={160} icon={<UserOutlined />} />;
    };

    const handleRedirectToProfile = () => {
        if (!isSignedIn) router.push('login');
        else router.push(routes.PROFILE);
    };

    const handleRedirectToUpload = () => {
        if (!isSignedIn) router.push('login');
        else router.push(routes.UPLOAD);
    };

    const handleRedirectToLiked = () => {
        if (!isSignedIn) router.push('login');
        else router.push(routes.LIKED_SONG);
    };

    const handleRedirectToHistory = () => {
        if (!isSignedIn) router.push('login');
        else router.push(routes.HISTORY);
    };

    const handleRedirectToPlaylist = () => {
        if (!isSignedIn) router.push(routes.LOGIN);
        else router.push(routes.PLAYLIST);
    };

    const handleRedirectToFollow = () => {
        if (!isSignedIn) router.push(routes.LOGIN);
        else router.push(routes.FOLLOW);
    };

    const handleRedirectToChat = () => {
        if (!isSignedIn) router.push(routes.LOGIN);
        else router.push(routes.CHAT);
    };

    if (isLoading) {
        return <Spin tip="Đang lấy thông tin tài khoản" size="large"></Spin>;
    }

    return (
        <div className={classes.root}>
            <div className={classes.info_container}>
                <div className={classes.avatar_container}>
                    <AvatarComponent />
                </div>
                <p className={classes.account_name}>{`${
                    profileData && profileData.first_name
                        ? profileData.first_name
                        : ''
                } ${
                    profileData && profileData.last_name
                        ? profileData.last_name
                        : ''
                }`}</p>

                <p className={classes.email}>
                    {profileData && profileData.email}
                </p>
            </div>
            <div className={classes.menu_container} id="sider_menu">
                <Menu
                    onClick={handleClick}
                    mode="inline"
                    className={classes.menu_bar}
                >
                    <Menu.Item
                        key="sub1"
                        title="Cá nhân"
                        onClick={handleRedirectToProfile}
                        className={
                            asPath === routes.PROFILE ? classes.active : ''
                        }
                    >
                        Sửa thông tin
                    </Menu.Item>
                    <Menu.Item
                        key="sub3"
                        title="Tải lên"
                        onClick={handleRedirectToUpload}
                        className={
                            asPath === routes.UPLOAD ? classes.active : ''
                        }
                    >
                        Tải lên
                    </Menu.Item>
                    <Menu.Item
                        key="sub4"
                        title="Theo dõi"
                        onClick={handleRedirectToFollow}
                        className={
                            asPath === routes.FOLLOW ? classes.active : ''
                        }
                    >
                        Theo dõi
                    </Menu.Item>
                    <Menu.Item
                        key="sub5"
                        title="Yêu thích"
                        onClick={handleRedirectToLiked}
                        className={
                            asPath === routes.LIKED_SONG ? classes.active : ''
                        }
                    >
                        Yêu thích
                    </Menu.Item>
                    <Menu.Item
                        key="sub6"
                        title="Lịch sử"
                        onClick={handleRedirectToHistory}
                        className={
                            asPath === routes.HISTORY ? classes.active : ''
                        }
                    >
                        Lịch sử
                    </Menu.Item>
                    <Menu.Item
                        key="sub7"
                        title="Playlist"
                        onClick={handleRedirectToPlaylist}
                        className={
                            asPath === routes.PLAYLIST ? classes.active : ''
                        }
                    >
                        Playlist
                    </Menu.Item>
                    <Menu.Item key="sub8" title="Cài đặt">
                        Cài đặt
                    </Menu.Item>
                    <Menu.Item
                        key="sub9"
                        title="Trò chuyện"
                        onClick={handleRedirectToChat}
                        className={asPath === routes.CHAT ? classes.active : ''}
                    >
                        Trò chuyện
                    </Menu.Item>
                    {isSignedIn && (
                        <Menu.Item
                            key="sub10"
                            title="Đăng xuất"
                            onClick={handleLogout}
                        >
                            Đăng xuất
                        </Menu.Item>
                    )}
                    {!isSignedIn && (
                        <Menu.Item
                            key="sub11"
                            title="Đăng nhập"
                            onClick={handleRedirectToLogin}
                        >
                            Đăng nhập
                        </Menu.Item>
                    )}
                </Menu>
            </div>
        </div>
    );
};

export default SideBar;
