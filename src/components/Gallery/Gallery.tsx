import React, { FC, useState, useCallback, Fragment } from 'react';
import classes from './styles.module.scss';
import { Row, Col, Spin, Space } from 'antd';
import Song from '../Song';
import { useQuery } from '@apollo/react-hooks';
import GET_ALL_SONG_QUERY from '../../queries/GetAllSong.graphql';
import { LoadingOutlined } from '@ant-design/icons';

const antIcon = (
    <LoadingOutlined style={{ fontSize: 24, color: 'white' }} spin />
);

interface ISong {
    id: string;
    playlist_id?: string;
    song_name: string;
    author?: string;
    song_url: string;
    song_image_url?: string;
    lyric?: string;
    like: number;
    comment: number;
    listen: number;
    uploader: string;
    duration?: number;
    listComment?: Array<string>;
}

const Gallery: FC = () => {
    const {
        data: songs,
        loading: songsLoading,
        error: songsError,
        refetch,
        fetchMore,
    } = useQuery(GET_ALL_SONG_QUERY, {
        fetchPolicy: 'network-only',
        variables: {
            limit: 8,
        },
    });

    const [isLoadingMore, setIsLoadingMore] = useState(false);

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    const onLoadMore = async () => {
        setIsLoadingMore(true);
        await sleep(1000);
        await fetchMore({
            variables: {
                page: Math.ceil(songs?.getAllSong.length / 8) + 1,
            },
        });
        setIsLoadingMore(false);
    };

    const handleScroll = async ({ currentTarget }, onLoadMore) => {
        if (
            currentTarget.scrollTop + currentTarget.clientHeight >=
            currentTarget.scrollHeight
        ) {
            await onLoadMore();
        }
    };

    if (songsLoading) {
        return <Spin tip="Get song..."></Spin>;
    }

    if (songsError) {
        return <div>Fetch song error...</div>;
    }

    if (!songs) {
        return <div>Fetch song error...</div>;
    }

    const data = songs.getAllSong;

    return (
        <Fragment>
            <div
                className={classes.root}
                onScroll={e => {
                    handleScroll(e, onLoadMore);
                }}
            >
                <Row
                    gutter={[
                        { xs: 8, sm: 16, md: 24, lg: 32 },
                        { xs: 24, sm: 24, md: 24, lg: 32 },
                    ]}
                >
                    {data.map((song, index) => {
                        return (
                            <Col
                                xs={24}
                                sm={24}
                                md={12}
                                lg={8}
                                xl={6}
                                key={index}
                            >
                                <Song data={song} refetch={refetch} />
                            </Col>
                        );
                    })}
                </Row>
                {isLoadingMore && (
                    <div className={classes.loading_more}>
                        <Spin tip="Đang tải thêm bài hát" indicator={antIcon} />
                    </div>
                )}
            </div>
        </Fragment>
    );
};

export default Gallery;
