import React, { FC, useState, useEffect } from 'react';
import classes from './styles/profile.module.scss';
import {
    Form,
    Input,
    InputNumber,
    Upload,
    message,
    DatePicker,
    Button,
    Radio,
    Spin,
    Modal,
    Row,
    Col,
} from 'antd';
import PropTypes from 'prop-types';
import Icon, {
    LoadingOutlined,
    PlusOutlined,
    UserOutlined,
    MailOutlined,
} from '@ant-design/icons';
import firebase from '../../firebase/firebase';
import md5 from 'md5';
import moment from 'moment';
import { useProfile } from '../../containers/profiles/useProfile';
import { checkSignedIn } from '../../utils/checkSignIn';
import { useRouter } from 'next/router';
import { url } from 'inspector';
import EditSvg from './edit.svg';
import * as routes from '../../constants/routes/routes';
import { useMediaQuery } from 'react-responsive';

interface ProfileProps {
    title: string;
}

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 16 },
};

const tailLayout = {
    wrapperCol: { offset: 4, span: 16 },
};

const dateFormat = 'DD/MM/YYYY';

const Profile: FC<ProfileProps> = ({ title }: ProfileProps) => {
    const router = useRouter();
    const isSignedIn = checkSignedIn();
    const isDesktopOrLaptop = useMediaQuery({
        query: '(min-width: 768px)',
    });

    useEffect(() => {
        if (!isSignedIn) {
            router.push('login');
        }
    });

    const [loading, setLoading] = useState(false);
    const [imageUrl, setImageUrl] = useState('');
    const [profileInfo, setProfileInfo] = useState({});
    const [isVisibleModal, setVisibleModal] = useState<boolean | undefined>(
        false
    );
    const [form] = Form.useForm();

    const handleCloseModal: VoidFunction = (): void => {
        setVisibleModal(false);
        router.push(routes.HOME);
    };
    const afterSubmit: VoidFunction = (): void => {
        setVisibleModal(true);
    };

    const hookProps = useProfile({ afterSubmit });

    const { profileData, isLoading, handleSubmit } = hookProps;

    useEffect(() => {
        if (profileData) {
            setImageUrl(profileData.avatarUrl);
            setProfileInfo(profileData);
        }
    }, [profileData]);

    const handleChange = info => {
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            console.log(info.file);
            // getBase64(info.file.originFileObj, imageUrl => {
            //     setLoading(false);
            //     setImageUrl(imageUrl);
            //     form.setFieldsValue({ avatarUrl: imageUrl });
            // });
        }
    };

    const beforeUpload = file => {
        const isJpgOrPng =
            file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
        }
        return isJpgOrPng && isLt2M;
    };
    const customUpload = async ({ onError, onSuccess, file }) => {
        const storage = firebase.storage();
        const metadata = {
            contentType: 'image/jpeg',
        };
        const storageRef = await storage.ref();
        const imageName = md5(file.uid); //a unique name for the image
        const imgFile = storageRef.child(`images/${imageName}.png`);
        try {
            const image = await imgFile.put(file, metadata);
            const imageUrl = await imgFile.getDownloadURL();
            setImageUrl(imageUrl);
            form.setFieldsValue({ avatarUrl: imageUrl });
            onSuccess(null, image);
        } catch (e) {
            onError(e);
        }
    };
    const uploadButton = (
        <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div style={{ marginTop: 8 }}>Upload</div>
        </div>
    );

    if (isLoading) {
        return (
            <div className={classes.loading_indicator}>
                <Spin tip="Đang lấy thông tin tài khoản" size="large"></Spin>
            </div>
        );
    }

    return (
        <div className={classes.root}>
            <Modal
                title="Chúc mừng"
                visible={isVisibleModal}
                onOk={handleCloseModal}
                footer={[
                    <Button
                        key="submit"
                        type="primary"
                        onClick={handleCloseModal}
                    >
                        Ok
                    </Button>,
                ]}
            >
                <p>Cập nhật tài khoản thành công</p>
            </Modal>
            <h1 className={classes.page_title}>Chỉnh sửa thông tin cá nhân</h1>
            <Row className={classes.avatar_container} id="upload_avatar">
                <Col>
                    <div className={classes.avatar_overlay}>
                        <Icon
                            component={EditSvg}
                            style={{ color: 'white', fontSize: `64px` }}
                        />
                    </div>
                    <Upload
                        name="avatar"
                        listType="picture-card"
                        className={classes.avatar_uploader}
                        showUploadList={false}
                        beforeUpload={beforeUpload}
                        onChange={handleChange}
                        customRequest={customUpload}
                    >
                        {imageUrl ? (
                            <img
                                src={imageUrl}
                                alt="avatar"
                                className={classes.avatar}
                            />
                        ) : (
                            uploadButton
                        )}
                    </Upload>
                </Col>
            </Row>
            <div className={classes.profile_form}>
                <Form
                    className={classes.form}
                    form={form}
                    layout="vertical"
                    onFinish={handleSubmit}
                    initialValues={
                        profileData
                            ? {
                                  ...profileData,
                                  birthday: moment(
                                      profileData.birthday,
                                      'YYYY/MM/DD'
                                  ),
                              }
                            : {}
                    }
                >
                    <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                        <Col md={24} lg={12}>
                            <Form.Item
                                label={
                                    <span className={classes.form__label}>
                                        Họ và chữ lót
                                    </span>
                                }
                                name="first_name"
                            >
                                {/* <div className={classes.form__input_container}> */}
                                <Input
                                    id="profile_first_name"
                                    size="large"
                                    placeholder="Họ và chữ lót"
                                    prefix={<UserOutlined />}
                                    className={classes.form__input_container}
                                    bordered={false}
                                />
                                {/* </div> */}
                            </Form.Item>
                            <Form.Item
                                name="birthday"
                                label={
                                    <span className={classes.form__label}>
                                        Năm sinh
                                    </span>
                                }
                            >
                                <DatePicker format={dateFormat} />
                            </Form.Item>
                        </Col>
                        <Col md={24} lg={12}>
                            <Form.Item
                                label={
                                    <span className={classes.form__label}>
                                        Tên
                                    </span>
                                }
                                name="last_name"
                            >
                                <Input
                                    id="profile_last_name"
                                    size="large"
                                    placeholder="Tên"
                                    prefix={<UserOutlined />}
                                    className={classes.form__input_container}
                                    bordered={false}
                                />
                            </Form.Item>
                            <Form.Item
                                name="gender"
                                label={
                                    <span className={classes.form__label}>
                                        Giới tính
                                    </span>
                                }
                            >
                                <Radio.Group>
                                    <Radio value={true}>Nam</Radio>
                                    <Radio value={false}>Nữ</Radio>
                                </Radio.Group>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Form.Item style={{ display: 'none' }} name="avatarUrl">
                        <Input />
                    </Form.Item>
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'center',
                        }}
                    >
                        <Form.Item
                            style={{
                                width: isDesktopOrLaptop ? '30%' : '100%',
                            }}
                        >
                            <Button
                                type="primary"
                                htmlType="submit"
                                className={classes.update_button}
                            >
                                Cập nhật
                            </Button>
                        </Form.Item>
                    </div>
                </Form>
            </div>
        </div>
    );
};

Profile.defaultProps = {
    title: '',
};

Profile.propTypes = {
    title: PropTypes.string.isRequired,
};

export default Profile;
