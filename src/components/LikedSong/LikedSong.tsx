import React, { FC, useEffect } from 'react';
import classes from './styles.module.scss';
import { useQuery } from '@apollo/react-hooks';
import LoadingIndicator from '@/components/base/loadingIndicator';
import SongItem from '@/components/UploadSong/Item';
import GET_LIKED_SONG_BY_CURRENT_ACCOUNT from '@/queries/GetLikedSongByCurrentAccount.graphql';

const LikedSong: FC<any> = () => {
    const { data: songs, loading, error } = useQuery(
        GET_LIKED_SONG_BY_CURRENT_ACCOUNT,
        {
            fetchPolicy: 'network-only',
        }
    );

    if (loading) return <LoadingIndicator loadingTitle="loading liked songs" />;
    if (error)
        return <div className={classes.error_page}>Error when fetch data</div>;
    if (!songs) {
        return <div className={classes.error_page}>Error when fetch data</div>;
    }

    let list: JSX.Element;

    if (songs) {
        list = (
            <div className={classes.content__list} id="list">
                <h1 className={classes.title}>Danh sách bài hát yêu thích</h1>
                {songs.getLikedSongByCurrentAccount.map(song => {
                    return <SongItem data={song} edit="disable"></SongItem>;
                })}
            </div>
        );
    }

    return <div className={classes.root}>{list}</div>;
};

export default LikedSong;
