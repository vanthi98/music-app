import React, { FC, useState, useRef } from 'react';
import classes from './styles.module.scss';
import {
    Menu,
    Upload,
    message,
    Row,
    Col,
    Form,
    Input,
    Button,
    Spin,
    Modal,
    Select,
} from 'antd';
import {
    InboxOutlined,
    LoadingOutlined,
    PlusOutlined,
} from '@ant-design/icons';
import firebase from '../../firebase/firebase';
import md5 from 'md5';
import { useUploadSong } from '../../containers/UploadSong/useUploadSong';
import { useQuery } from '@apollo/react-hooks';
import GET_UPLOADED_SONG_QUERY from '../../queries/GetSongByCurrentMember.graphql';
import GET_COUNTRY_QUERY from '@/queries/queries/GetCountries.graphql';
import SongItem from './Item';
import responsiveColumn from '@/utils/responsiveColumn';

const { Item } = Menu;
const { Dragger } = Upload;
const { TextArea } = Input;
const { Option } = Select;

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

const UploadSong: FC = () => {
    const [currentContent, setCurrentContent] = useState<string | undefined>(
        'upload'
    );
    const [loading, setLoading] = useState<boolean | undefined>();
    const [imageUrl, setImageUrl] = useState('');
    const [form] = Form.useForm();

    const [isModalVisible, setIsModalVisible] = useState<boolean | undefined>(
        false
    );

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
        setCurrentContent('list');
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const {
        data: songs,
        loading: getSongLoading,
        error: getSongError,
        refetch,
    } = useQuery(GET_UPLOADED_SONG_QUERY, { fetchPolicy: 'network-only' });

    const {
        data: countries,
        loading: getCountryLoading,
        error: getCountryError,
    } = useQuery(GET_COUNTRY_QUERY, { fetchPolicy: 'network-only' });

    const afterSubmit = () => {
        showModal();
        form.resetFields();
        setImageUrl('');
    };

    const hookProps = useUploadSong({ refetch, afterSubmit });

    const { loading: loadingUpload, handleSubmit } = hookProps;

    const handleChangeContent = e => {
        setCurrentContent(e.key);
    };

    const handleChangeImage = info => {
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            console.log(info.file);
            getBase64(info.file.originFileObj, imageUrl => {
                setLoading(false);
            });
        }
    };

    const beforeUploadSong = file => {
        const isMp3 = file.type === 'audio/mpeg';
        if (!isMp3) {
            message.error('You can only upload mp3 file!');
        }
        const isLt10M = file.size / 1024 / 1024 < 10;
        if (!isLt10M) {
            message.error('Song must smaller than 10MB!');
        }
        return isMp3 && isLt10M;
    };

    const beforeUploadImage = file => {
        const isJpgOrPng =
            file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
        }
        return isJpgOrPng && isLt2M;
    };

    const customRequest: Function = async ({ onError, onSuccess, file }) => {
        const storage = firebase.storage();
        const metadata = {
            contentType: 'audio/mpeg',
        };
        const storageRef = await storage.ref();
        const songFile = storageRef.child(`songs/${file.name}`);
        try {
            const song = await songFile.put(file, metadata);
            const song_url = await songFile.getDownloadURL();
            console.log(song_url);
            form.setFieldsValue({ song_url });
            onSuccess(null, song);
        } catch (e) {
            onError(e);
        }
    };

    const customUpload = async ({ onError, onSuccess, file }) => {
        const storage = firebase.storage();
        const metadata = {
            contentType: 'image/jpeg',
        };
        const storageRef = await storage.ref();
        const imageName = md5(file.uid); //a unique name for the image
        const imgFile = storageRef.child(`images/${imageName}.png`);
        try {
            const image = await imgFile.put(file, metadata);
            const imageUrl = await imgFile.getDownloadURL();
            setImageUrl(imageUrl);
            form.setFieldsValue({ song_image_url: imageUrl });
            onSuccess(null, image);
        } catch (e) {
            onError(e);
        }
    };

    const props: object = {
        name: 'file',
        multiple: false,
        onChange(info) {
            if (info.file.status === 'uploading') {
                return;
            }
            if (info.file.status === 'done') {
                return;
            }
        },
        customRequest,
        beforeUpload: beforeUploadSong,
    };

    const uploadButton = (
        <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div style={{ marginTop: 8 }}>Upload Song Image</div>
        </div>
    );

    function handleChange(value) {
        console.log(`selected ${value}`);
    }

    if (loadingUpload || getCountryLoading || getSongLoading) {
        return (
            <div className={classes.loading_indicator}>
                <Spin tip="Đang lấy thông tin tài khoản" size="large"></Spin>
            </div>
        );
    }

    const upload: JSX.Element = (
        <div className={classes.content__upload} id="upload_song">
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                <Col {...responsiveColumn(24, 24, 12, 12, 7, 7)}>
                    <Dragger
                        {...props}
                        className={classes.content__upload_song}
                    >
                        <p className="ant-upload-drag-icon">
                            <InboxOutlined />
                        </p>
                        <p className="ant-upload-text">
                            Click or drag file to this area to upload your song
                        </p>
                    </Dragger>
                </Col>
                <Col {...responsiveColumn(24, 24, 12, 12, 7, 7)}>
                    <Upload
                        name="avatar"
                        listType="picture-card"
                        className={classes.content__upload_image}
                        showUploadList={false}
                        beforeUpload={beforeUploadImage}
                        onChange={handleChangeImage}
                        customRequest={customUpload}
                    >
                        {imageUrl ? (
                            <img
                                src={imageUrl}
                                alt="avatar"
                                className={classes.content__song_image}
                            />
                        ) : (
                            uploadButton
                        )}
                    </Upload>
                </Col>
                <Col {...responsiveColumn(24, 24, 24, 24, 10, 10)}>
                    <h2>Thông tin bài hát</h2>
                    <Form
                        layout="vertical"
                        className={classes.form}
                        form={form}
                        onFinish={handleSubmit}
                    >
                        <Form.Item style={{ display: 'none' }} name="song_url">
                            <Input />
                        </Form.Item>
                        <Form.Item
                            style={{ display: 'none' }}
                            name="song_image_url"
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label={
                                <span className={classes.form__label}>
                                    Tên bài hát
                                </span>
                            }
                            name="song_name"
                        >
                            <div className={classes.form__input_container}>
                                <Input
                                    id="profile_first_name"
                                    size="large"
                                    placeholder="Tên bài hát"
                                    className={classes.first_name_input}
                                    bordered={false}
                                />
                            </div>
                        </Form.Item>
                        <Form.Item
                            label={
                                <span className={classes.form__label}>
                                    Tên tác giả
                                </span>
                            }
                            name="author"
                        >
                            <div className={classes.form__input_container}>
                                <Input
                                    id="profile_first_name"
                                    size="large"
                                    placeholder="Tên tác giả"
                                    className={classes.first_name_input}
                                    bordered={false}
                                />
                            </div>
                        </Form.Item>
                        <Form.Item
                            label={
                                <span className={classes.form__label}>
                                    Lời bài hát
                                </span>
                            }
                            name="lyric"
                        >
                            <div className={classes.form__input_container}>
                                <TextArea
                                    id="profile_first_name"
                                    placeholder="Lời bài hát"
                                    autoSize={{ minRows: 3, maxRows: 5 }}
                                    className={classes.first_name_input}
                                    bordered={false}
                                />
                            </div>
                        </Form.Item>
                        {getCountryLoading ? (
                            <Spin />
                        ) : countries ? (
                            <Form.Item
                                label={
                                    <span className={classes.form__label}>
                                        Quốc gia
                                    </span>
                                }
                                name="country"
                            >
                                <Select
                                    style={{ width: '100%' }}
                                    onChange={handleChange}
                                >
                                    {countries.getAllCountry.map(country => {
                                        return (
                                            <Option value={country.country_id}>
                                                {country.name}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </Form.Item>
                        ) : null}
                        <div
                            style={{
                                display: 'flex',
                                justifyContent: 'flex-end',
                            }}
                        >
                            <Form.Item>
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    className={classes.upload_button}
                                >
                                    Tải lên
                                </Button>
                            </Form.Item>
                        </div>
                    </Form>
                </Col>
            </Row>
            <Modal
                title="Chúc mừng"
                visible={isModalVisible}
                onOk={handleOk}
                onCancel={handleCancel}
            >
                <p>Upload bài hát thành công</p>
            </Modal>
        </div>
    );

    let list: JSX.Element;

    if (getSongLoading) {
        list = <Spin>Loading</Spin>;
    }

    if (getSongError) {
        list = <p>Error occured when get list</p>;
    }

    if (songs && !getSongLoading && !getSongError) {
        list = (
            <div className={classes.content__list} id="list">
                <h1 className={classes.title}>Danh sách tải lên</h1>
                {songs.getSongByCurrentAccount.map(song => {
                    return (
                        <SongItem data={song} countries={countries}></SongItem>
                    );
                })}
            </div>
        );
    }

    const content: JSX.Element = currentContent === 'upload' ? upload : list;

    return (
        <div className={classes.root}>
            <div className={classes.menu}>
                <Menu
                    mode="horizontal"
                    selectedKeys={[currentContent]}
                    onClick={handleChangeContent}
                >
                    <Item key="upload">Tải lên</Item>
                    <Item key="list">Danh sách</Item>
                </Menu>
            </div>
            <div className={classes.content}>{content}</div>
        </div>
    );
};

export default UploadSong;
