import React, { FC, useState, useCallback, Fragment, ReactNode } from 'react';
import classes from './item.module.scss';
import { useDispatch } from 'react-redux';
import {
    Row,
    Col,
    Tooltip,
    Modal,
    Form,
    Input,
    Button,
    Select,
    notification,
    Comment,
    Tag,
    List,
} from 'antd';
import Icon from '@ant-design/icons';
import ActionSvg from './action.svg';
import PlaylistSvg from './playlist.svg';
import DownloadSvg from './download.svg';
import PlaySvg from './play.svg';
import ToolsSvg from './tools.svg';
import HeartSvg from './heart.svg';
import ListenSvg from './listen.svg';
import CommentSvg from './comment.svg';
import LISTEN_SONG from '../../../queries/ListenSong.graphql';
import UPDATE_SONG from '@/queries/mutations/UpdateSong.graphql';
import GET_COMMENTS_QUERY from '@/queries/GetCommentBySong.graphql';
import CREATE_COMMENT_MUTATION from '@/queries/CreateComment.graphql';
import { useMutation, useQuery } from '@apollo/react-hooks';
import responsiveColumn from '../../../utils/responsiveColumn';
import moment from 'moment';
import TextEditor from '../../TextEditor';
import ScrollToBottom from 'react-scroll-to-bottom';

const { TextArea } = Input;
const { Option } = Select;

interface IComment {
    id: string;
    parent: string;
    actions: Array<ReactNode>;
    author: string;
    avatar: string;
    content: any;
    datetime: JSX.Element;
    children: Array<string>;
}

function createMarkup(html: string) {
    return { __html: html };
}

const Item: FC<any> = ({ data, edit, addtoplaylist, refetch, countries }) => {
    console.log(data);
    const dispatch = useDispatch();
    const [form] = Form.useForm();
    const [formComment] = Form.useForm();
    const [isModalVisible, setIsModalVisible] = useState<boolean | undefined>(
        false
    );

    const [isCommentModalVisible, setCommentModalVisible] = useState<
        boolean | undefined
    >(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const showCommentModal = () => {
        setCommentModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleCommentOK = () => {};

    const handleCommentCancel = () => {
        setCommentModalVisible(false);
    };

    const [listenSongMutation, { error: listenError }] = useMutation(
        LISTEN_SONG
    );

    const [updateSong, { error: updateSongError }] = useMutation(UPDATE_SONG);

    const [replyTo, setReplyTo] = useState('');

    const {
        data: listComment,
        loading: loadingComment,
        error: errorComment,
        refetch: getCommentRefetch,
    } = useQuery(GET_COMMENTS_QUERY, {
        variables: { song_id: data._id },
        fetchPolicy: 'network-only',
    });

    const [
        createCommentMutation,
        { loading: createCommentLoading, error: createCommentError },
    ] = useMutation(CREATE_COMMENT_MUTATION);

    const handleSubmitComment = useCallback(
        async formValues => {
            try {
                const { title, content, parent, replyTo } = formValues;
                let variables = {
                    song_id: data._id,
                    title,
                    content,
                };
                if (parent && replyTo) {
                    variables['parent_id'] = parent;
                    variables['replyTo'] = replyTo;
                }
                await createCommentMutation({
                    variables,
                });

                if (getCommentRefetch) {
                    getCommentRefetch();
                }

                formComment.resetFields();
                setReplyTo('');
            } catch (error) {
                console.log(error);
            }
        },
        [createCommentMutation, getCommentRefetch, replyTo]
    );

    const handleReply: Function = (account_name: string, id: string): void => {
        setReplyTo(account_name);
        form.setFieldsValue({ parent: id, replyTo: account_name });
    };

    const handleRemoveReply = () => {
        setReplyTo('');
        form.setFieldsValue({ parent: '', replyTo: '' });
    };

    const handleUpdateSong = useCallback(
        async formValues => {
            if (data)
                updateSong({
                    variables: {
                        song_id: data._id,
                        input: formValues,
                    },
                });

            setIsModalVisible(false);
            notification.success({
                message: `Chỉnh sửa bài hát thành công`,
            });
        },
        [data]
    );

    const commentModal: Function = (): JSX.Element => {
        if (!listComment) return null;
        const comments: Array<IComment> = listComment.getCommentBySong.map(
            comment => {
                return {
                    id: comment._id,
                    parent: comment.parent,
                    children: comment.children,
                    actions: [
                        <span
                            key="comment-list-reply-to-0"
                            onClick={() =>
                                handleReply(
                                    comment.user.account_name,
                                    comment.parent
                                        ? comment.parent
                                        : comment._id
                                )
                            }
                        >
                            Phản hồi
                        </span>,
                    ],
                    author: comment.user.account_name,
                    avatar: comment.user.avatarUrl,
                    content: (
                        <Fragment>
                            <p style={{ fontWeight: 'bold' }}>
                                {comment.title}
                            </p>
                            {comment.replyTo && (
                                <Tag color="cyan">
                                    Trả lời {`@${comment.replyTo}`}
                                </Tag>
                            )}
                            <div
                                dangerouslySetInnerHTML={createMarkup(
                                    comment.content
                                )}
                            />
                        </Fragment>
                    ),
                    datetime: (
                        <Tooltip
                            title={moment(comment.createdAt).format(
                                'YYYY-MM-DD HH:mm:ss'
                            )}
                        >
                            <span>{moment(comment.createdAt).fromNow()}</span>
                        </Tooltip>
                    ),
                };
            }
        );
        const Editor = ({ onSubmit }) => (
            <Form onFinish={onSubmit} form={formComment}>
                <Form.Item name="title">
                    <Input placeholder="Chủ đề" />
                </Form.Item>
                <Form.Item name="replyTo" style={{ display: 'none' }}>
                    <Input />
                </Form.Item>
                {replyTo && (
                    <Form.Item name="parent" label="Trả lời">
                        <Tag closable onClose={handleRemoveReply}>
                            {`@${replyTo}`}
                        </Tag>
                    </Form.Item>
                )}
                <Form.Item name="content">
                    <TextEditor
                        placeholder="Nội dung"
                        onChange={() => {}}
                        value=""
                    />
                </Form.Item>
                <Form.Item style={{ marginBottom: 0 }}>
                    <Button htmlType="submit" type="primary">
                        Thêm bình luận
                    </Button>
                </Form.Item>
            </Form>
        );
        return (
            <div className={classes.comment}>
                <List
                    className={classes.list_comment}
                    header={`${comments.length} lượt bình luận`}
                    itemLayout="horizontal"
                    dataSource={comments}
                    renderItem={item => {
                        const { parent, id } = item;
                        const childComment = comments
                            .filter(comment => {
                                return comment.parent === id;
                            })
                            .map(comment => {
                                return (
                                    <Comment
                                        content={comment.content}
                                        actions={[
                                            <span
                                                key="comment-list-reply-to-0"
                                                onClick={() =>
                                                    handleReply(
                                                        comment.author,
                                                        comment.parent
                                                    )
                                                }
                                            >
                                                Phản hồi
                                            </span>,
                                        ]}
                                        author={comment.author}
                                        avatar={comment.avatar}
                                        datetime={comment.datetime}
                                    />
                                );
                            });
                        if (!parent) {
                            return (
                                <li>
                                    <Comment
                                        actions={item.actions}
                                        author={item.author}
                                        avatar={item.avatar}
                                        content={item.content}
                                        datetime={item.datetime}
                                    >
                                        {childComment}
                                    </Comment>
                                </li>
                            );
                        }
                    }}
                />
                <p className={classes.add_comment_text}>Thêm bình luận</p>
                <Comment content={<Editor onSubmit={handleSubmitComment} />} />
            </div>
        );
    };

    return (
        <div className={classes.root}>
            <Row
                gutter={[{ xs: 16, sm: 16, md: 24 }, 16]}
                align="middle"
                justify="space-between"
                style={{ margin: 0 }}
            >
                <Col
                    className={`${classes.info_container} gutter-row`}
                    {...responsiveColumn(24, 24, 8, 8, 8, 8)}
                >
                    <img
                        src={
                            data.song_image_url ||
                            'https://via.placeholder.com/150'
                        }
                        alt={data.name}
                        width={50}
                        height={50}
                        className={classes.song_picture}
                    />
                    <div className={classes.title_and_artist_container}>
                        <p className={classes.title}>{data.song_name}</p>
                        <p className={classes.artist}>{data.author}</p>
                    </div>
                </Col>
                <Col>
                    <Tooltip placement="top" title="Phát bài hát này">
                        <Icon
                            component={PlaySvg}
                            onClick={async () => {
                                localStorage.setItem(
                                    'song',
                                    JSON.stringify(data)
                                );
                                await dispatch({
                                    type: 'SET_CURRENT_TIME',
                                    payload: 0,
                                });
                                await dispatch({
                                    type: 'SET_CURRENT_SONG',
                                    payload: {
                                        song_url: data.song_url,
                                        song_image_url: data.song_image_url,
                                        song_name: data.song_name,
                                        artist: data.author,
                                        like: data.like,
                                        listen: data.listen,
                                        uploader: data.uploader,
                                        song_id: data._id,
                                    },
                                });
                                await listenSongMutation({
                                    variables: {
                                        song_id: data._id,
                                    },
                                });
                                if (refetch) {
                                    refetch();
                                }
                            }}
                            className={classes.icon}
                        />
                    </Tooltip>
                </Col>
                {addtoplaylist !== 'disable' && (
                    <Col>
                        <Tooltip
                            placement="top"
                            title="Thêm bài hát này vào playlist"
                        >
                            <Icon
                                component={PlaylistSvg}
                                className={classes.icon}
                            />
                        </Tooltip>
                    </Col>
                )}
                <Col>
                    <Tooltip placement="top" title="Tải xuống">
                        <Icon
                            component={DownloadSvg}
                            onClick={() => {
                                fetch(
                                    'https://cors-anywhere.herokuapp.com/' +
                                        data.song_url,
                                    {
                                        method: 'GET',
                                        headers: {
                                            'Content-Type': 'audio/mpeg',
                                        },
                                    }
                                )
                                    .then(response => response.blob())
                                    .then(blob => {
                                        if (process.browser) {
                                            const url = window.URL.createObjectURL(
                                                new Blob([blob])
                                            );
                                            const link = document.createElement(
                                                'a'
                                            );
                                            link.href = url;
                                            link.setAttribute(
                                                'download',
                                                `${data.song_name}.mp3`
                                            );

                                            document.body.appendChild(link);

                                            link.click();

                                            link.parentNode.removeChild(link);
                                        }
                                    });
                            }}
                            className={classes.icon}
                        />
                    </Tooltip>
                </Col>
                {edit !== 'disable' && (
                    <Col>
                        <Tooltip placement="top" title="Chỉnh sửa">
                            <Icon
                                component={ToolsSvg}
                                className={classes.icon}
                                onClick={showModal}
                            />
                        </Tooltip>
                    </Col>
                )}
                <Col>
                    <Tooltip placement="top" title="Thông tin">
                        <Icon
                            component={ActionSvg}
                            className={classes.icon}
                            onClick={() => {
                                Modal.info({
                                    title: 'Thông tin bài hát',
                                    content: (
                                        <div className={classes.info_modal}>
                                            <p
                                                className={
                                                    classes.text_in_modal
                                                }
                                            >
                                                Tên bài hát: {data.song_name}{' '}
                                            </p>
                                            <p
                                                className={
                                                    classes.text_in_modal
                                                }
                                            >
                                                Nghệ sĩ: {data.author}{' '}
                                            </p>
                                            <p
                                                className={
                                                    classes.text_in_modal
                                                }
                                            >
                                                Lượt nghe: {data.listen}{' '}
                                                <Icon
                                                    component={ListenSvg}
                                                    className={
                                                        classes.icon_in_modal
                                                    }
                                                />
                                            </p>
                                            <p
                                                className={
                                                    classes.text_in_modal
                                                }
                                            >
                                                Lượt like: {data.like}{' '}
                                                <Icon
                                                    component={HeartSvg}
                                                    className={
                                                        classes.icon_in_modal
                                                    }
                                                />
                                            </p>
                                        </div>
                                    ),
                                });
                            }}
                        />
                    </Tooltip>
                </Col>
                <Col>
                    <Tooltip placement="top" title="Bình luận">
                        <Icon
                            component={CommentSvg}
                            className={classes.icon}
                            onClick={() => {
                                showCommentModal();
                            }}
                        />
                    </Tooltip>
                </Col>
            </Row>
            <Modal
                title="Bình luận"
                visible={isCommentModalVisible}
                onOk={() => {
                    setCommentModalVisible(false);
                }}
                onCancel={handleCommentCancel}
            >
                {commentModal()}
            </Modal>
            <Modal
                title="Chỉnh sửa bài hát"
                visible={isModalVisible}
                onCancel={handleCancel}
                footer={[
                    <Button
                        type="primary"
                        htmlType="submit"
                        onClick={handleCancel}
                    >
                        Hủy
                    </Button>,
                ]}
            >
                <Form
                    form={form}
                    onFinish={handleUpdateSong}
                    initialValues={{
                        song_name: data.song_name,
                        author: data.author,
                        lyric: data.lyric,
                        country: data.country,
                    }}
                >
                    <Form.Item name="song_name">
                        <Input placeholder="Nhập tên bài hát" />
                    </Form.Item>
                    <Form.Item name="author">
                        <Input placeholder="Nhập tên tác giả" />
                    </Form.Item>
                    <Form.Item name="lyric">
                        <TextArea
                            placeholder="Lời bài hát"
                            autoSize={{ minRows: 3, maxRows: 5 }}
                            bordered={true}
                        />
                    </Form.Item>
                    <Form.Item
                        label={
                            <span className={classes.form__label}>
                                Quốc gia
                            </span>
                        }
                        name="country"
                    >
                        <Select
                            defaultValue={
                                countries?.getAllCountry[0].country_id
                            }
                            style={{ width: '100%' }}
                        >
                            {countries?.getAllCountry.map(country => {
                                return (
                                    <Option value={country.country_id}>
                                        {country.name}
                                    </Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                    <Form.Item style={{ marginTop: '1rem' }}>
                        <Button type="primary" htmlType="submit" size="large">
                            Cập nhật
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
};

export default Item;
