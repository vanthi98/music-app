import React, { FC, useCallback, useEffect, useState } from 'react';
import classes from './styles.module.scss';
import { useRouter } from 'next/router';
import { useMutation, useQuery, useLazyQuery } from '@apollo/react-hooks';
import GET_PROFILE_QUERY from '../../queries/queries/GetProfile.graphql';
import GET_UPLOADED_SONG_QUERY from '../../queries/queries/GetSongById.graphql';
import FOLLOW_MUTATION from '../../queries/Follow.graphql';
import UN_FOLLOW_MUTATION from '@/queries/UnFollow.graphql';
import CHECK_FOLLOW from '../../queries/CheckFollow.graphql';
import LoadingIndicator from '../base/loadingIndicator';
import { Row, Col, Button, Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import SongItem from '../UploadSong/Item';

const UserProfile: FC<any> = () => {
    const router = useRouter();
    const { id } = router.query;
    const { data: profile, loading, error, refetch } = useQuery(
        GET_PROFILE_QUERY,
        {
            variables: {
                id: id,
            },
            fetchPolicy: 'network-only',
        }
    );

    const [
        runFollowQuery,
        { data: isFollow, loading: isFollowLoading, error: isFollowError },
    ] = useLazyQuery(CHECK_FOLLOW, {
        fetchPolicy: 'network-only',
    });

    const {
        data: songs,
        loading: getSongLoading,
        error: getSongError,
    } = useQuery(GET_UPLOADED_SONG_QUERY, {
        variables: {
            id,
        },
        fetchPolicy: 'network-only',
    });

    const [
        followMutation,
        { loading: followLoading, error: followError },
    ] = useMutation(FOLLOW_MUTATION);

    const [
        unFollowMutation,
        { loading: unFollowLoading, error: unFollowError },
    ] = useMutation(UN_FOLLOW_MUTATION);

    const handleFollow = useCallback(async () => {
        if (profile) {
            await followMutation({
                variables: {
                    follow_id: profile.getProfile.id,
                },
            });
            runFollowQuery({
                variables: {
                    follow_id: profile.getProfile.id,
                },
            });

            refetch();
        }
    }, [followMutation, profile, isFollow]);

    const handleUnFollow = useCallback(async () => {
        if (profile) {
            await unFollowMutation({
                variables: {
                    follow_id: profile.getProfile.id,
                },
            });
            runFollowQuery({
                variables: {
                    follow_id: profile.getProfile.id,
                },
            });

            refetch();
        }
    }, [unFollowMutation, profile, isFollow]);

    useEffect(() => {
        if (profile)
            runFollowQuery({
                variables: {
                    follow_id: profile.getProfile.id,
                },
            });
    }, [profile]);

    if (loading || getSongLoading)
        return <LoadingIndicator loadingTitle="Loading profile" />;
    if (error || getSongError)
        return <div className={classes.error_page}>Error when fetch data</div>;
    if (!profile || !songs) {
        return <div className={classes.error_page}>Error when fetch data</div>;
    }
    const profileData = profile.getProfile;

    const checkFollow = isFollow ? isFollow.checkFollow : undefined;

    let list: JSX.Element;

    if (songs && !getSongLoading && !getSongError) {
        list = (
            <div className={classes.content__list} id="list">
                <h1 className={classes.title}>Danh sách tải lên</h1>
                {songs.getSongById.map(song => {
                    return <SongItem data={song} edit="disable"></SongItem>;
                })}
            </div>
        );
    }

    const AvatarComponent: FC = () => {
        if (profileData) {
            const { avatarUrl, last_name } = profileData;
            if (!avatarUrl)
                return (
                    <Avatar size={160}>
                        <p className={classes.avatar_text}>{last_name[0]}</p>
                    </Avatar>
                );
            return (
                <Avatar src={profileData && profileData.avatarUrl} size={160} />
            );
        }
        return <Avatar size={160} icon={<UserOutlined />} />;
    };

    return (
        <div className={classes.root}>
            <div className={classes.profile_info}>
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                    <Col>
                        <AvatarComponent />
                    </Col>
                    <Col>
                        <h1
                            className={classes.profile_name}
                        >{`${profileData.first_name} ${profileData.last_name}`}</h1>
                        <p className={classes.profile_account}>
                            Tên tài khoản:{' '}
                            <span style={{ color: '#333333' }}>
                                {profileData.account_name}
                            </span>
                        </p>
                        <div className={classes.follow_container}>
                            <p className={classes.profile_follow}>
                                Theo dõi:{' '}
                                <span style={{ color: '#333333' }}>
                                    {profileData.listFollowers.length}
                                </span>
                            </p>
                            <p className={classes.profile_follow}>
                                Đang theo dõi:{' '}
                                <span style={{ color: '#333333' }}>
                                    {profileData.listFollowings.length}
                                </span>
                            </p>
                        </div>
                        {checkFollow !== 'yourself' && (
                            <div className={classes.action_container}>
                                <Button
                                    className={classes.follow_button}
                                    onClick={async () => {
                                        checkFollow === 'true'
                                            ? handleUnFollow()
                                            : handleFollow();
                                    }}
                                >
                                    {checkFollow === 'true'
                                        ? 'Bỏ theo dõi'
                                        : 'Theo dõi'}
                                </Button>
                                <Button className={classes.block_button}>
                                    Chặn
                                </Button>
                            </div>
                        )}
                    </Col>
                </Row>
            </div>
            {list}
        </div>
    );
};

export default UserProfile;
