const checkSignedIn = () => {
    const ISSERVER = typeof window === 'undefined';
    if (!ISSERVER) return !!localStorage.getItem('token');
};

export { checkSignedIn };
