const parseTime = (time: number): string => {
    let result: string = '';
    if (time < 60) {
        if (time < 10) result = `00:0${time}`;
        else result = `00:${time}`;
    }
    if (60 < time) {
        const minute: number = Math.floor(time / 60);
        const second: number = time - minute * 60;
        const secondString: string = second >= 10 ? `${second}` : `0${second}`;
        if (minute < 10) {
            result = `0${minute}:${secondString}`;
        } else {
            result = `${minute}:${secondString}`;
        }
    }
    return result;
};

export default parseTime;
