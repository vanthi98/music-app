const responsiveColumn = (xs, sm, md, lg, xl, xxl) => {
    return {
        xs,
        sm,
        md,
        lg,
        xl,
        xxl,
    };
};

export const responsiveColumnWithOffset = (xs, sm, md, lg, xl, xxl) => {};

export default responsiveColumn;
