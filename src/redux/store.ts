import { createStore, AnyAction } from 'redux';
import { MakeStore, createWrapper, Context, HYDRATE } from 'next-redux-wrapper';
import { combineReducers } from 'redux';
import { testReducer } from './store/test/reducer';
import { songReducer } from './store/music/reducer';

const rootReducer = combineReducers({
    test: testReducer,
    song: songReducer,
});

export interface State {
    test: {
        title: string;
    };
    song: {
        song: {
            song_url: string;
        };
        currentTime: number;
    };
}

const makeStore: MakeStore<State> = (context: Context) =>
    createStore(rootReducer);
export const wrapper = createWrapper<State>(makeStore, { debug: false });
