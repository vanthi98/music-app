import { Test, CHANGE_TITLE, titleActionsType } from './types'

export function changeTitle(title: Test): titleActionsType {
    return {
        type: CHANGE_TITLE,
        payload: title,
    }
}
