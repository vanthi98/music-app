import { Test, CHANGE_TITLE, titleActionsType } from './types'

const initialState: Test = {
    title: 'React',
}

export function testReducer(
    state = initialState,
    action: titleActionsType
): Test {
    switch (action.type) {
        case CHANGE_TITLE:
            return { title: action.payload.title }
        default:
            return state
    }
}
