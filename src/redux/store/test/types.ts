export const CHANGE_TITLE = 'CHANGE_TITLE'

export interface Test {
    title: string
}

export interface changeTitleAction {
    type: typeof CHANGE_TITLE
    payload: Test
}

export type titleActionsType = changeTitleAction
