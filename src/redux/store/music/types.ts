export const SET_CURRENT_SONG = 'SET_CURRENT_SONG';
export const SET_CURRENT_TIME = 'SET_CURRENT_TIME';

export interface Song {
    song_id: string;
    song_url: string;
    song_image_url: string;
    song_name: string;
    artist: string;
    like: number;
    listen: number;
    uploader: string;
}

export interface setCurrentSong {
    type: typeof SET_CURRENT_SONG;
    payload: Song;
}

export interface setCurrentTime {
    type: typeof SET_CURRENT_TIME;
    payload: number;
}

export type setSongActionsType = setCurrentSong | setCurrentTime;
