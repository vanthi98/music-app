import {
    Song,
    SET_CURRENT_SONG,
    setSongActionsType,
    SET_CURRENT_TIME,
} from './types';

export function setCurrentSong(song: Song): setSongActionsType {
    return {
        type: SET_CURRENT_SONG,
        payload: song,
    };
}

export function setCurrentTime(time: number): setSongActionsType {
    return {
        type: SET_CURRENT_TIME,
        payload: time,
    };
}
