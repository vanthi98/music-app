import {
    Song,
    SET_CURRENT_SONG,
    setSongActionsType,
    SET_CURRENT_TIME,
} from './types';

interface ISongState {
    song: Song;
    currentTime: number;
}

const initialState: ISongState = {
    song: {
        song_id: '',
        song_url: '',
        song_image_url: '',
        song_name: '',
        artist: '',
        listen: 0,
        like: 0,
        uploader: '',
    },
    currentTime: 0,
};

export function songReducer(
    state = initialState,
    action: setSongActionsType
): ISongState {
    switch (action.type) {
        case SET_CURRENT_SONG:
            if (action.payload.song_url === state.song.song_url) {
                return {
                    ...state,
                    song: {
                        song_id: action.payload.song_id,
                        song_url: action.payload.song_url,
                        song_image_url: action.payload.song_image_url,
                        song_name: action.payload.song_name,
                        artist: action.payload.artist,
                        like: action.payload.like,
                        listen: action.payload.listen,
                        uploader: action.payload.uploader,
                    },
                    currentTime: 0,
                };
            }
            return {
                ...state,
                song: {
                    song_id: action.payload.song_id,
                    song_url: action.payload.song_url,
                    song_image_url: action.payload.song_image_url,
                    song_name: action.payload.song_name,
                    artist: action.payload.artist,
                    like: action.payload.like,
                    listen: action.payload.listen,
                    uploader: action.payload.uploader,
                },
            };
        case SET_CURRENT_TIME:
            return {
                ...state,
                currentTime: action.payload,
            };
        default:
            return state;
    }
}
