import firebase from 'firebase/app';
import 'firebase/storage';

var firebaseConfig = {
    apiKey: 'AIzaSyAFKdrmN5kEPznqK8lc6XCWbRWuhvMOix0',
    authDomain: 'upload-image-40e6e.firebaseapp.com',
    projectId: 'upload-image-40e6e',
    storageBucket: 'upload-image-40e6e.appspot.com',
    messagingSenderId: '402304580102',
    appId: '1:402304580102:web:6b6bd48e1cba964520e187',
    measurementId: 'G-0Q72XYVW61',
};
// Initialize Firebase
// const storage = firebase.storage();
// //analytics is optional for this tutoral

// export { storage };
export default !firebase.apps.length
    ? firebase.initializeApp(firebaseConfig)
    : firebase.app();
