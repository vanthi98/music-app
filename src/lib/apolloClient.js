import { useMemo } from 'react';
import { ApolloClient, HttpLink, InMemoryCache, split } from '@apollo/client';
import { concatPagination } from '@apollo/client/utilities';
import merge from 'deepmerge';
import { setContext } from '@apollo/client/link/context';
import { getMainDefinition } from '@apollo/client/utilities';
import { WebSocketLink } from '@apollo/client/link/ws';
import ws from 'ws';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import { offsetLimitPagination } from '@apollo/client/utilities';

export const APOLLO_STATE_PROP_NAME = '__APOLLO_STATE__';

let apolloClient;

let token;
if (typeof window !== 'undefined') {
    token = localStorage.getItem('token');
}

const httpLink = new HttpLink({
    uri: 'https://mecha-music-backend.herokuapp.com/graphql', // Server URL (must be absolute)
    credentials: 'include', // Additional fetch() options like `credentials` or `headers`
    headers: {
        authorization: token ? `Bearer ${token}` : '',
    },
});

// const client = new SubscriptionClient('ws://localhost:4000/subscriptions', {
//     reconnect: true,
// });

// const wsLink = new WebSocketLink({
//     // if you instantiate in the server, the error will be thrown
//     uri: 'wss://mecha-music-backend.herokuapp.com/graphql',
//     options: {
//         lazy: true,
//         reconnect: true,
//         connectionParams: () => {
//             return {
//                 headers: {
//                     authorization: token ? `Bearer ${token}` : '',
//                 },
//             };
//         },
//     },
//     // webSocketImpl: wss,
// });
const wsLink = new SubscriptionClient(
    'wss://mecha-music-backend.herokuapp.com/graphql',
    { reconnect: true },
    ws
);
// : new WebSocketLink({
//       uri: 'wss://mecha-music-backend.herokuapp.com/graphql',
//       options: {
//           lazy: true,
//           reconnect: true,
//           connectionParams: () => {
//               return {
//                   headers: {
//                       authorization: token ? `Bearer ${token}` : '',
//                   },
//               };
//           },
//       },
//   });

const splitLink = process.browser
    ? split(
          ({ query }) => {
              const definition = getMainDefinition(query);
              return (
                  definition.kind === 'OperationDefinition' &&
                  definition.operation === 'subscription'
              );
          },
          wsLink,
          httpLink
      )
    : httpLink;

function createApolloClient() {
    return new ApolloClient({
        ssrMode: typeof window === 'undefined',
        link: splitLink,
        cache: new InMemoryCache({
            typePolicies: {
                Query: {
                    fields: {
                        getAllSong: offsetLimitPagination(),
                    },
                },
            },
        }),
    });
}

export function initializeApollo(initialState = null) {
    const _apolloClient = apolloClient ?? createApolloClient();

    // If your page has Next.js data fetching methods that use Apollo Client, the initial state
    // gets hydrated here
    if (initialState) {
        // Get existing cache, loaded during client side data fetching
        const existingCache = _apolloClient.extract();

        // Merge the existing cache into data passed from getStaticProps/getServerSideProps
        const data = merge(initialState, existingCache);

        // Restore the cache with the merged data
        _apolloClient.cache.restore(data);
    }
    // For SSG and SSR always create a new Apollo Client
    if (typeof window === 'undefined') return _apolloClient;
    // Create the Apollo Client once in the client
    if (!apolloClient) apolloClient = _apolloClient;

    return _apolloClient;
}

export function addApolloState(client, pageProps) {
    if (pageProps?.props) {
        pageProps.props[APOLLO_STATE_PROP_NAME] = client.cache.extract();
    }

    return pageProps;
}

export function useApollo(pageProps) {
    const state = pageProps[APOLLO_STATE_PROP_NAME];
    const store = useMemo(() => initializeApollo(state), [state]);
    return store;
}
