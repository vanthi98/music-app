import React from 'react';
import Profile from '../components/profile';
import Layout from '../components/layout';
import { NextPage } from 'next';
import { wrapper, State } from '../redux/store';
import { checkSignedIn } from '../utils/checkSignIn';
import { useRouter } from 'next/router';

const ProfilePage: NextPage = () => {
    const router = useRouter();
    return (
        <>
            <Layout>
                <Profile title={'profile page'} />
            </Layout>
        </>
    );
};

export default ProfilePage;
