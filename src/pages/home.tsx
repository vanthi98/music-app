import React from 'react';
import { NextPage } from 'next';
import Layout from '../components/layout';
import Gallery from '../components/Gallery';

const HomePage: NextPage = () => {
    return (
        <Layout>
            <Gallery />
        </Layout>
    );
};

export default HomePage;
