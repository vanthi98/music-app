import React from 'react';
import { NextPage } from 'next';
import Layout from '../../components/layout';
import UploadSong from '../../components/UploadSong';

const UploadSongPage: NextPage = () => {
    return (
        <Layout>
            <UploadSong />
        </Layout>
    );
};

export default UploadSongPage;
