import React from 'react';
import { NextPage } from 'next';
import Layout from '@/components/layout';
import CountryDetail from '@/components/CountryDetail';

const CountryDetailPage: NextPage = () => {
    return (
        <Layout>
            <CountryDetail />
        </Layout>
    );
};

export default CountryDetailPage;
