import React from 'react';
import Login from '../components/login';
import { NextPage } from 'next';
import { useSelector, useDispatch } from 'react-redux';
import { wrapper, State } from '../redux/store';
import { checkSignedIn } from '../utils/checkSignIn';
import { useRouter } from 'next/router';

const LoginPage: NextPage = () => {
    const router = useRouter();
    if (checkSignedIn()) {
        router.push('home');
    }
    return (
        <>
            <Login title={'đăng nhập'} />
        </>
    );
};

export default LoginPage;
