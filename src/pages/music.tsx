import React from 'react';
import Music from '../components/music';
import { NextPage } from 'next';

const MusicPage: NextPage = () => {
    return <Music />;
};

export default MusicPage;
