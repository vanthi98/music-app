import React from 'react';
import { NextPage } from 'next';
import Layout from '../components/layout';
import Ranking from '@/components/Ranking';

const RankingPage: NextPage = () => {
    return (
        <Layout>
            <Ranking />
        </Layout>
    );
};

export default RankingPage;
