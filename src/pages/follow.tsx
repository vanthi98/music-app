import React from 'react';
import { NextPage } from 'next';
import Follow from '@/components/Follow';
import Layout from '@/components/layout';

const LikedSongPage: NextPage = () => {
    return (
        <Layout>
            <Follow />
        </Layout>
    );
};

export default LikedSongPage;
