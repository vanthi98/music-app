import Head from 'next/head';
import styles from '../styles/Home.module.css';
import LoadingIndicator from '../components/base/loadingIndicator';

export default function Home() {
    return <LoadingIndicator loadingTitle={'Loading...'} />;
}
