import React from 'react';
import { NextPage } from 'next';
import LikedSong from '@/components/LikedSong';
import Layout from '@/components/layout';

const LikedSongPage: NextPage = () => {
    return (
        <Layout>
            <LikedSong />
        </Layout>
    );
};

export default LikedSongPage;
