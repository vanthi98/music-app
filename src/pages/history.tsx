import React from 'react';
import { NextPage } from 'next';
import HistorySong from '@/components/History';
import Layout from '@/components/layout';

const LikedSongPage: NextPage = () => {
    return (
        <Layout>
            <HistorySong />
        </Layout>
    );
};

export default LikedSongPage;
