import React from 'react';
import SignUp from '../components/signup';
import { NextPage } from 'next';
import { wrapper, State } from '../redux/store';
import { checkSignedIn } from '../utils/checkSignIn';
import { useRouter } from 'next/router';

const SignUpPage: NextPage = () => {
    const router = useRouter();
    if (checkSignedIn()) {
        router.push('home');
    }
    return (
        <>
            <SignUp title={'đăng ký'} />
        </>
    );
};

export default SignUpPage;
