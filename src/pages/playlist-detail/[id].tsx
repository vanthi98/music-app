import React from 'react';
import { NextPage } from 'next';
import Layout from '@/components/layout';
import PlaylistDetail from '@/components/PlaylistDetail';

const PlaylistDetailPage: NextPage = () => {
    return (
        <Layout>
            <PlaylistDetail />
        </Layout>
    );
};

export default PlaylistDetailPage;
