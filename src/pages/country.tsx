import React from 'react';
import { NextPage } from 'next';
import Country from '@/components/Country';
import Layout from '@/components/layout';

const LikedSongPage: NextPage = () => {
    return (
        <Layout>
            <Country />
        </Layout>
    );
};

export default LikedSongPage;
