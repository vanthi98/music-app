import React from 'react';
import { NextPage } from 'next';
import Layout from '../components/layout';
import Search from '@/components/Search';

const SearchPage: NextPage = () => {
    return (
        <Layout>
            <Search />
        </Layout>
    );
};

export default SearchPage;
