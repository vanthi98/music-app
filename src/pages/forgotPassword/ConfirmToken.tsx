import React from 'react';
import { NextPage } from 'next';
import ConfirmToken from '../../components/ForgotPassword/ConfirmToken';

const ForgotPasswordPage: NextPage = () => {
    return <ConfirmToken />;
};

export default ForgotPasswordPage;
