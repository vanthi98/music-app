import React from 'react';
import { NextPage } from 'next';
import ResetPassword from '../../components/ForgotPassword/ResetPassword';

const ResetPasswordPage: NextPage = () => {
    return <ResetPassword />;
};

export default ResetPasswordPage;
