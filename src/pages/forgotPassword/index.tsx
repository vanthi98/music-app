import React from 'react';
import { NextPage } from 'next';
import ForgotPassword from '../../components/ForgotPassword';

const ForgotPasswordPage: NextPage = () => {
    return <ForgotPassword></ForgotPassword>;
};

export default ForgotPasswordPage;
