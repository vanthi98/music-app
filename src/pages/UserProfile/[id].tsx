import React from 'react';
import { NextPage } from 'next';
import UserProfile from '@/components/UserProfile';
import Layout from '@/components/layout';

const UserProfilePage: NextPage = () => {
    return (
        <Layout>
            <UserProfile />
        </Layout>
    );
};

export default UserProfilePage;
