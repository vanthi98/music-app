import React, { FC, useState } from 'react';
import Router from 'next/router';
import { ApolloProvider } from '@apollo/client';
import { useApollo } from '../lib/apolloClient';
import { AppProps } from 'next/app';
import dynamic from 'next/dynamic';
import { wrapper } from '../redux/store';
import '../styles/globals.css';
import '../styles/global.scss';
import 'antd/dist/antd.css';
import NextNProgress from 'next-nprogress/component';
import LoadingIndicator from '../components/base/loadingIndicator';
import Music from '../components/music';
import { useSelector } from 'react-redux';

const ISSERVER = typeof window === 'undefined';
let song;

if (!ISSERVER) {
    song = JSON.parse(localStorage.getItem('song'));
}

const WrappedApp: FC<AppProps> = ({ Component, pageProps }) => {
    const apolloClient = useApollo(pageProps);
    const [loading, setLoading] = useState(false);
    const currentSong = useSelector(state => state.song.song.song_url);
    React.useEffect(() => {
        const start = () => {
            console.log('start');
            setLoading(true);
        };
        const end = () => {
            console.log('findished');
            setLoading(false);
        };
        Router.events.on('routeChangeStart', start);
        Router.events.on('routeChangeComplete', end);
        Router.events.on('routeChangeError', end);
        return () => {
            Router.events.off('routeChangeStart', start);
            Router.events.off('routeChangeComplete', end);
            Router.events.off('routeChangeError', end);
        };
    }, []);

    let content: JSX.Element = loading ? (
        <LoadingIndicator loadingTitle={'Loading...'} />
    ) : (
        <Component {...pageProps} />
    );
    return (
        <ApolloProvider client={apolloClient}>
            {content}
            {currentSong && <Music />}
        </ApolloProvider>
    );
};

export default wrapper.withRedux(WrappedApp);
