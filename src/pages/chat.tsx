import React from 'react';
import { NextPage } from 'next';
import Layout from '../components/layout';
import Chat from '@/components/Chat';

const ChatPage: NextPage = () => {
    return (
        <Layout>
            <Chat />
        </Layout>
    );
};

export default ChatPage;
