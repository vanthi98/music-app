import React from 'react'
import Test from '../components/test'
import { NextPage } from 'next'
import { useSelector, useDispatch } from 'react-redux'
import { wrapper, State } from '../redux/store'

const TestPage: NextPage = () => {
    const { title } = useSelector<State, State>((state) => state.test)
    const dispatch = useDispatch()
    return (
        <>
            <button
                onClick={() => {
                    dispatch({
                        type: 'CHANGE_TITLE',
                        payload: {
                            title: 'Angular',
                        },
                    })
                }}
            >
                Change title
            </button>
            <Test title={title} />
        </>
    )
}

export default TestPage
