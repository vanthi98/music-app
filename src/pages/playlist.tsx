import React from 'react';
import { NextPage } from 'next';
import Layout from '@/components/layout';
import Playlist from '@/components/Playlist';

const PlaylistPage: NextPage = () => {
    return (
        <Layout>
            <Playlist />
        </Layout>
    );
};

export default PlaylistPage;
