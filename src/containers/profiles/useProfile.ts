import { useCallback, useEffect, useState } from 'react';
import { useMutation, useQuery, useLazyQuery } from '@apollo/react-hooks';
import GET_PROFILE_QUERY from '../../queries/getProfile.graphql';
import GET_CURRENT_ACCOUNT_ID_QUERY from '../../queries/getCurrentAccountId.graphql';
import UPDATE_PROFILE_MUTATION from '../../queries/updateProfile.graphql';

export const useProfile = props => {
    const { afterSubmit } = props;
    const {
        data: accountIdData,
        loading: accountIdLoading,
        error: accountIdError,
        refetch,
    } = useQuery(GET_CURRENT_ACCOUNT_ID_QUERY, {
        fetchPolicy: 'network-only',
    });

    const [
        runGetProfileQuery,
        { data: profile, loading: getProfileLoading, error: getProfileError },
    ] = useLazyQuery(GET_PROFILE_QUERY, {
        fetchPolicy: 'network-only',
    });

    const [
        updateProfileMutation,
        {
            data: updateProfile,
            loading: updateProfileLoading,
            error: updateProfileError,
        },
    ] = useMutation(UPDATE_PROFILE_MUTATION);

    let accountId;
    if (!accountIdLoading && !accountIdError && accountIdData) {
        accountId = accountIdData.getCurrentAccountId;
    }

    useEffect(() => {
        if (accountId) {
            runGetProfileQuery({
                variables: {
                    id: accountId,
                },
            });
        }
    }, [accountId, runGetProfileQuery]);

    let profileData;
    let profileId;
    if (!getProfileLoading && profile) {
        profileData = profile.getProfileByAccountId;
        profileId = profileData.id;
    }

    const handleSubmit = useCallback(
        async formValues => {
            try {
                console.log(formValues);
                const { birthday } = formValues;
                const birthdayString = birthday ? birthday._d : '';
                if (profileId) {
                    await updateProfileMutation({
                        variables: {
                            ...formValues,
                            birthday: birthdayString,
                            id: profileId,
                        },
                    });

                    if (afterSubmit) {
                        afterSubmit();
                    }
                }
            } catch (error) {
                console.log(error);
            }
        },
        [profileId]
    );
    return {
        refetch,
        isLoading: accountIdLoading || getProfileLoading,
        accountId,
        profileData,
        profileId,
        handleSubmit,
        runGetProfileQuery,
    };
};
