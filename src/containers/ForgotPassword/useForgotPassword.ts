import { useCallback, useState, useEffect } from 'react';
import { useMutation } from '@apollo/react-hooks';
import FORGOT_PASSWORD from '../../queries/ResetPassword.graphql';
import CONFIRM_TOKEN from '../../queries/ConfirmToken.graphql';
import RESET_ACCOUNT_PASSWORD from '../../queries/ResetAccountPassword.graphql';
import DELETE_RESET_PASSWORD_TOKEN from '../../queries/DeleteResetPasswordToken.graphql';

export const useForgotPassword = props => {
    const { afterSubmit } = props;
    const [sendMailMutation, { data: id, loading, error }] = useMutation(
        FORGOT_PASSWORD
    );
    const [mail, setMail] = useState<string | undefined>('');
    const [timesLeft, setTimesLeft] = useState<number | undefined>(5);
    const [zeroTimeError, setZeroTimeError] = useState<boolean | undefined>(
        false
    );

    const [
        confirmTokenMutation,
        { data: isSuccess, loading: confirmLoading, error: confirmError },
    ] = useMutation(CONFIRM_TOKEN);

    const [
        resetPasswordMutation,
        { loading: resetLoading, error: resetError },
    ] = useMutation(RESET_ACCOUNT_PASSWORD);

    const [
        deleteResetPasswordToken,
        { loading: deleteTokenLoading, error: deleteTokenError },
    ] = useMutation(DELETE_RESET_PASSWORD_TOKEN);

    const handleSubmit = useCallback(
        async formValues => {
            try {
                const { email } = formValues;
                if (email) {
                    setMail(email);
                    await sendMailMutation({
                        variables: {
                            input: email,
                        },
                    });
                    if (afterSubmit) {
                        afterSubmit();
                    }
                }
            } catch (error) {
                console.log(error);
            }
        },
        [sendMailMutation]
    );

    const handleConfirmToken = useCallback(
        async formValues => {
            try {
                const { token } = formValues;
                if (token && timesLeft > 0) {
                    await confirmTokenMutation({
                        variables: {
                            token,
                        },
                    });
                    localStorage.setItem('RESET_PASSWORD_TOKEN', token);
                    if (afterSubmit) {
                        afterSubmit();
                    }
                }
            } catch (error) {
                console.log(error);
            }
        },
        [confirmTokenMutation]
    );

    const handleResetPassword = useCallback(async formValues => {
        try {
            const { password } = formValues;
            const token = localStorage.getItem('RESET_PASSWORD_TOKEN');
            if (password && token) {
                await resetPasswordMutation({
                    variables: {
                        password,
                        token,
                    },
                });
                localStorage.removeItem('RESET_PASSWORD_TOKEN');
                if (afterSubmit) {
                    afterSubmit();
                }
            }
        } catch (error) {
            console.log(error);
        }
    }, []);

    useEffect(() => {
        if (confirmError && timesLeft > 0) {
            setTimesLeft(timesLeft => timesLeft - 1);
        }
    }, [confirmError, setTimesLeft]);

    useEffect(() => {
        if (timesLeft === 0) {
            setZeroTimeError(true);
            deleteResetPasswordToken({
                variables: {
                    email: mail,
                },
            });
        }
    }, [timesLeft]);

    return {
        id,
        loading,
        error,
        isSuccess,
        confirmLoading,
        confirmError,
        resetLoading,
        resetError,
        handleSubmit,
        handleConfirmToken,
        handleResetPassword,
        timesLeft,
        zeroTimeError,
    };
};
