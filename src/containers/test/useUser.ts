import { useState, useEffect, useCallback, useMemo } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import USER_QUERY from '../../queries/getUser.graphql';
import LOGIN_MUTATION from '../../queries/login.graphql';
import GET_ACCOUNT_QUERY from '../../queries/getUser.graphql';
import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

export const useUser = props => {
    const {
        loading: accountLoading,
        error: accountError,
        data: allAccountData,
    } = useQuery(GET_ACCOUNT_QUERY, {
        fetchPolicy: 'network-only',
    });

    const [
        loginMutation,
        { loading: loginLoading, error: loginError, data: loginData },
    ] = useMutation(LOGIN_MUTATION);

    const loading = accountLoading;
    const errors: Array<any> = [];

    if (accountError) {
        errors.push(accountError.graphQLErrors[0]);
    }

    const allAccount = useMemo(() => {
        if (!allAccountData) {
            return null;
        }
        return allAccountData.getAllAccount;
    }, [allAccountData]);

    const handleLogin = useCallback(
        async formValues => {
            try {
                const { account_name, password } = formValues;
                const result = await loginMutation({
                    variables: {
                        account_name,
                        password,
                    },
                });
                const { access_token } = result.data.login;
                localStorage.setItem('token', access_token);
            } catch (error) {
                console.log(error);
            }
        },
        [loginMutation]
    );

    return {
        allAccount,
        errors,
        handleLogin,
        loading,
        loginError,
        loginLoading,
    };
};
