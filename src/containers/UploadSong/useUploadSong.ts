import { useCallback, useEffect } from 'react';
import { useMutation, useQuery, useLazyQuery } from '@apollo/react-hooks';
import UPLOAD_SONG_MUTATION from '../../queries/UploadSong.graphql';

export const useUploadSong = props => {
    const { afterSubmit, refetch } = props;
    console.log(afterSubmit);
    const [
        uploadSongMutation,
        {
            data: uploadSongData,
            loading: uploadSongLoading,
            error: uploadSongError,
        },
    ] = useMutation(UPLOAD_SONG_MUTATION);

    const handleSubmit = useCallback(async formValues => {
        try {
            console.log(formValues);

            await uploadSongMutation({
                variables: {
                    ...formValues,
                },
            });

            if (afterSubmit) {
                afterSubmit();
            }
            if (refetch) {
                refetch();
            }
        } catch (error) {
            console.log(error);
        }
    }, []);
    return {
        loading: uploadSongLoading,
        data: uploadSongData,
        error: uploadSongError,
        handleSubmit,
    };
};
