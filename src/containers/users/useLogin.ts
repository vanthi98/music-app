import { useCallback } from 'react';
import { useMutation } from '@apollo/react-hooks';
import LOGIN_MUTATION from '../../queries/login.graphql';

export const useLogin = props => {
    const { afterSubmit } = props;
    const [
        loginMutation,
        { loading: loginLoading, error: loginError, data: loginData },
    ] = useMutation(LOGIN_MUTATION);

    const errors: Array<string> = [];

    if (loginError && loginError.graphQLErrors.length > 0) {
        loginError.graphQLErrors.forEach(graphQLError => {
            errors.push(graphQLError.message);
        });
    }

    const handleLogin = useCallback(
        async formValues => {
            try {
                const { email, password } = formValues;
                const result = await loginMutation({
                    variables: {
                        email,
                        password,
                    },
                });
                const { access_token } = result.data.login;
                localStorage.removeItem('token');
                localStorage.setItem('token', access_token);
                if (afterSubmit && access_token) {
                    afterSubmit();
                }
            } catch (error) {
                console.log(error);
            }
        },
        [loginMutation, afterSubmit]
    );

    return {
        errors,
        handleLogin,
        loginError,
        loginData,
        loginLoading,
    };
};
