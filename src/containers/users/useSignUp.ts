import { useCallback } from 'react';
import { useMutation } from '@apollo/react-hooks';
import CREATE_ACCOUNT_MUTATION from '../../queries/createAccount.graphql';
import timeout from '../../utils/timeout';

export const useSignUp = props => {
    const { afterSubmit } = props;
    const [
        createAccountMutation,
        {
            loading: createAccountLoading,
            error: createAccountError,
            data: createAccountData,
        },
    ] = useMutation(CREATE_ACCOUNT_MUTATION);

    const errors: Array<string> = [];

    if (createAccountError && createAccountError.graphQLErrors.length > 0) {
        createAccountError.graphQLErrors.forEach(graphQLError => {
            errors.push(graphQLError.message);
        });
    }

    const handleCreateAccount = useCallback(
        async formValues => {
            try {
                const {
                    account_name,
                    password,
                    email,
                    gender,
                    birthday,
                    first_name,
                    last_name,
                } = formValues;
                const birthdayString = birthday ? birthday._d : '';
                await createAccountMutation({
                    variables: {
                        account_name,
                        password,
                        email,
                        gender,
                        birthday: birthdayString,
                        first_name,
                        last_name,
                    },
                });

                if (afterSubmit) {
                    afterSubmit();
                }
            } catch (error) {
                console.log(error);
            }
        },
        [createAccountMutation, afterSubmit]
    );

    return {
        errors,
        handleCreateAccount,
        createAccountError,
        createAccountData,
        createAccountLoading,
    };
};
