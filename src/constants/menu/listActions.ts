import FacebookSvg from '../../../public/static/icons/facebook.svg';
import TwitterSvg from '../../../public/static/icons/twitter.svg';
import InstagramSvg from '../../../public/static/icons/instagram.svg';
import GoogleSvg from '../../../public/static/icons/google.svg';

interface ActionIconItem {
    id: number;
    component: any;
    color?: string;
    hoverColor?: string;
    size?: number;
}

const ICON_SIZE: number = 16;
const ICON_COLOR: string = '#d3d3d3';
const ICON_HOVER_COLOR: string = '#fe0098';

export const listAction: Array<ActionIconItem> = [
    {
        id: 1,
        component: FacebookSvg,
        color: ICON_COLOR,
        hoverColor: ICON_HOVER_COLOR,
        size: ICON_SIZE,
    },
    {
        id: 2,
        component: GoogleSvg,
        color: ICON_COLOR,
        hoverColor: ICON_HOVER_COLOR,
        size: ICON_SIZE,
    },
];
