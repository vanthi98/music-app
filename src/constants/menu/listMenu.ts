import * as routes from '@/constants/routes/routes';

interface TabMenuItem {
    tab: string;
    key: string;
    url?: string;
}

export const defaultActiveMenu: string = 'home';

export const listMenu: Array<TabMenuItem> = [
    {
        tab: 'Trang chủ',
        key: 'home',
        url: routes.HOME,
    },
    {
        tab: 'Bảng xếp hạng',
        key: 'ranking',
        url: routes.RANKING,
    },
    {
        tab: 'Quốc gia',
        key: 'country',
        url: routes.COUNTRY,
    },
    {
        tab: 'Thể loại',
        key: 'category',
    },
    {
        tab: 'Liên hệ',
        key: 'contact',
    },
    {
        tab: 'Thông tin',
        key: 'information',
    },
];
