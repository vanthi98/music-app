module.exports = {
    trailingComma: 'es5',
    tabWidth: 4,
    semi: true,
    singleQuote: true,
    arrowParens: 'avoid',
    overrides: [
        {
            files: '*.scss',
            options: {
                tabWidth: 2,
            },
        },
    ],
};
